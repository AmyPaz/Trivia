# Trivia

Trivia game that is based on a server-client model.
The server is written in C++ and communicates with an SQLite database. The client is a WPF application written in C#.


## Installation
1. Please make sure that you have `SQLite` installed on your computer.
2. To run the game, build both the `Trivia` and `ClientGUI` solutions. 
3. After the `.exe` files for the server and client are created, you may move the relevant files to whichever folder you desire:
 
(A) Server:
- The server's `.exe` file can be found at `\trivia\Trivia\x64\Debug`.
- Make sure to include both `Trivia.exe` and `TriviaBackupDB.sqlite` in the same directory when executing the server program for the first time.
- For each execution after the first execution of the server program, make sure to include `TriviaDB.sqlite` in the same directory as `Trivia.exe`.

(B) Client:
- The client's `.exe` file can be found at `\trivia\ClientGUI\ClientGUI\bin\Debug\net5.0-windows`.
- Make sure to copy the entire folder in which `ClientGUI.exe` is located (`net5.0-windows`). For greater convenience, you may create a shortcut to the `.exe` file itself.
 


## Usage
1. Execute `Server.exe`.
2. Once the server is up and running, you may execute `ClientGUI.exe`. Each window represents an independent user.
3. To shut the server down, enter "EXIT" in the server's console window.