import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn


def create_question(conn, question):
    """
    Add a question to the DB
    :param conn: the connection object of the DB
    :param question: question and answers
    :type tuple
    :return: none
    """
    if len(question) == 3:
        sql = " INSERT INTO questions(question,ans1,ans2) VALUES('" + question[0] + "','" + question[1] + "','" + \
              question[2]
    elif len(question) == 4:
        sql = " INSERT INTO questions(question,ans1,ans2,ans3) VALUES('" + question[0] + "','" + question[1] + "','" \
             + question[2] + "','" + question[3]
    elif len(question) == 5:
        sql = " INSERT INTO questions(question,ans1,ans2,ans3,ans4) VALUES('" + question[0] + "','" + question[1] + \
              "','" + question[2] + "','" + question[3] + "','" + question[4]
    sql += "')"
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()


def main():
    database = r"Trivia\TriviaDB.sqlite"
    temp = input("Enter DB path or 'default' for: 'Trivia\\TriviaDB.sqlite': \n")
    if temp != database:
        database = temp
    # create a database connection
    conn = create_connection(database)
    with conn:
        print("""\nYou must enter the question and at least 2 answers
Type exit in the third or fourth answer to not enter it
Type exit as the question to leave\n""")
        while True:
            quest = (input("Enter question: "),)
            if quest[0] == 'exit':
                exit()
            else:
                quest += (input("Enter the correct answer: "),)
                quest += (input("Enter the second answer: "),)
                temp = input("Enter the third answer: ")
                if temp != 'exit':
                    quest += (temp,)
                    temp = input("Enter the fourth answer: ")
                    if temp != 'exit':
                        quest += (temp,)
            create_question(conn, quest)
            print()


if __name__ == '__main__':
    main()
