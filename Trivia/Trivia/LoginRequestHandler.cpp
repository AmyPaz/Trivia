#include "LoginRequestHandler.h"

/*
A constructor function. This function creates a new "LoginRequestHandler" type object.
Input: handleFactory - The handle factory that will be used by the object.
Output: None.
*/
LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory& handlerFactory)
    : m_handlerFactory(handlerFactory), m_loginManager(m_handlerFactory.getLoginManager())
{  
}

/*
A destructor function. Frees the memory that was allocated by the constructor.
Input: None.
Output: None.
*/
LoginRequestHandler::~LoginRequestHandler()
{
}

/*
This function checks whether or not a request is relevant (whether or not it is a login request or a signup request).
Input: request - The request that will be checked.
Output: boolean - Whether or not the request is relevant.
*/
bool LoginRequestHandler::isRequestRelevant(RequestInfo& request) const
{
    return (request.requestId == LOGIN_REQUEST_CODE || request.requestId == SIGNUP_REQUEST_CODE);
}

/*
This function returns the result that is suitable for the request that a client sent.
Input: request - The request that the client sent.
Output: The result of the request (buffer that contains the result, the next handle to be used).
*/
RequestResult LoginRequestHandler::handleRequest(RequestInfo& request) 
{
    RequestResult result;
    if (request.requestId == LOGIN_REQUEST_CODE)
    {
        result = login(request);
    }
    else if (request.requestId == SIGNUP_REQUEST_CODE)
    {
        result = signup(request);
    }

    return result;
}

/*
This function handles the loss of connection with the client.
Input: None.
Output: None.
*/
void LoginRequestHandler::connectionLost()
{
}

/*
This function uses the input that was sent by the user to log him into the system.
Input: request - The login request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult LoginRequestHandler::login(RequestInfo request)
{
    //Deserializes the request.
    LoginRequest loginRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(request.buffer);

    //The result that will be returned from the function.
    RequestResult result;

    //The code of response.
    LoginResponse response;

    try
    {
        m_loginManager.login(loginRequest.username, loginRequest.password);

        //Sets the values of the response.
        response.status = SUCCESS_CODE;
        result.response = JsonResponsePacketSerializer::serializeResponse(response);
        result.newHandler = m_handlerFactory.createMenuRequestHandler(LoggedUser(loginRequest.username));
    }
    catch (std::exception& e)
    {
        ErrorResponse errorResponse;
        //Sets the values of the response.
        errorResponse.message = e.what();
        result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = nullptr;
    }
    return result;
}

/*
This function uses the input that was sent by the user to sign him up to the system.
Input: request - The signup request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult LoginRequestHandler::signup(RequestInfo request)
{
    //Deserializes the request.
    SignupRequest signupRequest = JsonRequestPacketDeserializer::deserializeSignupRequest(request.buffer);

    //The result that will be returned from the function.
    RequestResult result;

    //The code of response.
    SignupResponse response;

    try
    {
        m_loginManager.signup(signupRequest.username, signupRequest.password, signupRequest.email);
        m_loginManager.login(signupRequest.username, signupRequest.password);

        //Sets the values of the response.
        response.status = SUCCESS_CODE;
        result.response = JsonResponsePacketSerializer::serializeResponse(response);
        result.newHandler = m_handlerFactory.createMenuRequestHandler(LoggedUser(signupRequest.username));
    }
    catch (std::exception& e)
    {
        ErrorResponse errorResponse;
        //Sets the values of the response.
        errorResponse.message = e.what();
        result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = nullptr;
    }
    return result;
}