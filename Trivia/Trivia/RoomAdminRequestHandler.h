#pragma once
#include "IRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"


class RequestHandlerFactory;
class RoomAdminRequestHandler :
    public IRequestHandler
{
public:
    RoomAdminRequestHandler(RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory, LoggedUser user, Room room);
    ~RoomAdminRequestHandler() = default;
    virtual bool isRequestRelevant(RequestInfo& request) const override;
    virtual RequestResult handleRequest(RequestInfo& request) override;
    virtual void connectionLost() override;

private:
    Room m_room;
    LoggedUser m_user;
    RoomManager& m_roomManager;
    RequestHandlerFactory& m_handlerFactory;

    RequestResult closeRoom(RequestInfo requestInfo);
    RequestResult startGame(RequestInfo requestInfo);
    RequestResult getRoomState(RequestInfo requestInfo);
};

