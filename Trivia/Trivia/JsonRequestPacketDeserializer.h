#pragma once
#include <iostream>
#include <vector>
#include "json.hpp"
using json = nlohmann::json;

//The size of a byte in bits.
#define BYTE_SIZE 8

//The size of the "message length" field in the buffer that the user sends.
#define MESSAGE_BYTES_NUM 4 

typedef struct LoginRequest
{
	std::string username;
	std::string password;
} LoginRequest;

typedef struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
} SignupRequest;

typedef struct GetPlayersInRoomRequest
{
	unsigned int roomId;
} GetPlayersInRoomRequest;

typedef struct JoinRoomRequest
{
	unsigned int roomId;
} JoinRoomRequest;

typedef struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
} CreateRoomRequest;

typedef struct SubmitAnswerRequest
{
	unsigned int answer;
} SubmitAnswerRequest;

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest (const std::vector<unsigned char> buffer);
	static SignupRequest deserializeSignupRequest(const std::vector<unsigned char> buffer);
	static GetPlayersInRoomRequest deserializeGetPlayersRequest(const std::vector<unsigned char> buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(const std::vector<unsigned char> buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(const std::vector<unsigned char> buffer);
	static SubmitAnswerRequest deserializerSubmitAnswerRequest(const std::vector<unsigned char> buffer);
	static long bin4byteToInt(const std::vector<unsigned char> buffer);
	static long bin1byteToInt(const std::vector<unsigned char> buffer);
};
void from_json(const json& j, LoginRequest& loginRequest);
void from_json(const json& j, SignupRequest& signupRequest);
void from_json(const json& j, GetPlayersInRoomRequest& getPlayersInRoomRequest);
void from_json(const json& j, JoinRoomRequest& joinRoomRequest);
void from_json(const json& j, CreateRoomRequest& createRoomRequest);
void from_json(const json& j, SubmitAnswerRequest& submitAnswerRequest);



