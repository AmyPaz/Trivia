#include "GameRequestHandler.h"

/*
A constructor function. This function creates a new "GameRequestHandler" type object.
Input: game - The game that the user participates in.
	   user - The user.
	   gameManager - The game manager that will be used by the handler to manage the game.
	   requestHandlerFactory - The request handler factory that will be used by the handler to create new handles.
Output: None.
*/
GameRequestHandler::GameRequestHandler(Game game, LoggedUser user, GameManager& gameManager, RequestHandlerFactory& handlerFactory) :
	m_game(game), m_user(user), m_gameManager(gameManager), m_handlerFactory(handlerFactory), m_questionSentTime(0)
{
}

/*
This function checks whether or not a request is relevant.
Input: request - The request that will be checked.
Output: boolean - Whether or not the request is relevant.
*/
bool GameRequestHandler::isRequestRelevant(RequestInfo& request) const
{
	return (request.requestId == LEAVE_GAME_REQUEST_CODE || request.requestId == GET_QUESTION_REQUEST_CODE ||
		request.requestId == SUBMIT_ANSWER_REQUEST_CODE || request.requestId == GET_GAME_RESULT_REQUEST_CODE);
}

/*
This function returns the result that is suitable for the request that a client sent.
Input: request - The request that the client sent.
Output: The result of the request (buffer that contains the result, the next handle to be used).
*/
RequestResult GameRequestHandler::handleRequest(RequestInfo& request)
{
    RequestResult result;
    switch (request.requestId)
    {
    case LEAVE_GAME_REQUEST_CODE:
        result = leaveGame(request);
        break;
    case GET_QUESTION_REQUEST_CODE:
        result = getQuestion(request);
        break;
    case SUBMIT_ANSWER_REQUEST_CODE:
        result = submitAnswer(request);
        break;
    case GET_GAME_RESULT_REQUEST_CODE:
        result = getGameResults(request);
        break;
    default:
        throw std::exception(__FUNCTION__);
    }
    return result;
}

/*
This function handles the loss of connection with the client.
Input: None.
Output: None.
*/
void GameRequestHandler::connectionLost()
{
    m_handlerFactory.getRoomManager().removeUserFromRoom(m_game.getGameId(), m_user);
    if (!m_handlerFactory.getRoomManager().roomExists(m_game.getGameId()))
    {
        m_gameManager.deleteGame(m_game.getGameId());
    }
    else
    {
        m_gameManager.playerLeft(m_game.getGameId(), m_user);
    }
    m_handlerFactory.getLoginManager().logout(m_user.getUsername());
}

/*
This function returns to the user the next question that he has to answer + the possible answer to that questions.
Input: request - The "GetQuestions" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult GameRequestHandler::getQuestion(RequestInfo request)
{
    RequestResult result;
    GetQuestionResponse response;
    try
    { 
        Question question = m_gameManager.getQuestionForUser(m_game.getGameId(), m_user);
        response.question = question.getQuestion();
        response.answers = question.getPossibleAnswers();
        m_questionSentTime = std::time(nullptr); //Used to track the time it took the user to answer the question.
        response.status = SUCCESS_CODE;
        result.response = JsonResponsePacketSerializer::serializeResponse(response);
        result.newHandler = nullptr;
    }
    catch (const std::exception& e)
    {
        ErrorResponse errorResponse;
        errorResponse.message = "Failed to retrive a question.";
        result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = nullptr;
    }
    return result;
}

/*
This function submit the user's answer to a question.
Input: request - The "SubmitAnswer" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult GameRequestHandler::submitAnswer(RequestInfo request)
{
    RequestResult result;
    SubmitAnswerResponse response;
    try
    {
        SubmitAnswerRequest submitAnswerRequest = JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(request.buffer);
        m_gameManager.submitAnswer(m_game.getGameId(), m_user, submitAnswerRequest.answer, difftime(request.receivalTime, m_questionSentTime));
        response.correctAnswerId = CORRECT_ANSWER_INDEX;
        response.status = SUCCESS_CODE;
        result.response = JsonResponsePacketSerializer::serializeResponse(response);
        result.newHandler = nullptr;
    }
    catch (const std::exception& e)
    {
        ErrorResponse errorResponse;
        errorResponse.message = "Failed to submit the answer.";
        result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = nullptr;
    }
    return result;
}

/*
This function returns the game's final results.
Input: request - The "GetGameResults" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult GameRequestHandler::getGameResults(RequestInfo request)
{
    RequestResult result;
    GetGameResultsResponse response;
    try
    {
        response.results = m_gameManager.getGameResults(m_game.getGameId());
        response.status = SUCCESS_CODE;
        result.response = JsonResponsePacketSerializer::serializeResponse(response);
        result.newHandler = nullptr;
    }
    catch (const std::exception& e)
    {
        ErrorResponse errorResponse;
        errorResponse.message = "Failed to retrive the results of the game.";
        result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = nullptr;
    }
    return result;
}

/*
This function disconnects the user from the game.
Input: request - The "LeaveGame" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult GameRequestHandler::leaveGame(RequestInfo request)
{
    RequestResult result;
    LeaveGameResponse response;
    try
    {   
        m_handlerFactory.getRoomManager().removeUserFromRoom(m_game.getGameId(), m_user);
        if (!m_handlerFactory.getRoomManager().roomExists(m_game.getGameId()))
        {
            m_gameManager.deleteGame(m_game.getGameId());
        }
        else
        {
            m_gameManager.playerLeft(m_game.getGameId(), m_user);
        }
        response.status = SUCCESS_CODE;
        result.response = JsonResponsePacketSerializer::serializeResponse(response);
        result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
    }
    catch (const std::exception& e)
    {
        ErrorResponse errorResponse;
        errorResponse.message = e.what();
        result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = nullptr;
    }
    return result;
}
