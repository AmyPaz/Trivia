#include "MenuRequestHandler.h"

/*
A constructor function. This function creates a new "MenuRequestHandler" type object.
Input: roomManager - The room manager that will be used by the handler to manage rooms.
       statisticsManager - The statistics manager that will be used by the handler to return statistics.
       requestHandlerFactory - The request handler factory that will be used by the handler to create new handles.
       user - The user.
       database - The game db.
Output: None.
*/
MenuRequestHandler::MenuRequestHandler(RoomManager& roomManager, StatisticsManager& statisticsManager, RequestHandlerFactory &requestHandlerFactory, LoggedUser user, IDatabase* database) :
m_roomManager(roomManager), m_statisticsManager(statisticsManager), m_handlerFactory(requestHandlerFactory), m_user(user), m_database(database)
{
}

/*
A destructor function. This function fress the memory that was allocated by the constructor.
Input: None.
Output: None.
*/
MenuRequestHandler::~MenuRequestHandler()
{
}

/*
This function checks whether or not a request is relevant.
Input: request - The request that will be checked.
Output: boolean - Whether or not the request is relevant.
*/
bool MenuRequestHandler::isRequestRelevant(RequestInfo& request) const
{
    return (request.requestId == SIGNOUT_REQUEST_CODE || request.requestId == GET_ROOMS_REQUEST_CODE ||
        request.requestId == GET_PLAYERS_REQUEST_CODE || request.requestId == GET_STATISTICS_REQUEST_CODE ||
        request.requestId == JOIN_ROOM_REQUEST_CODE || request.requestId == CREATE_ROOM_REQUEST_CODE);
}

/*
This function handles the request based on its content.
Input: request - The request that was sent by the user.
Output: The result of the request (response to the client + next handle of the socket).
*/
RequestResult MenuRequestHandler::handleRequest(RequestInfo& request)
{
    RequestResult result;
    switch (request.requestId)
    {
        case SIGNOUT_REQUEST_CODE:
            result = signout(request);
            break;
        case GET_ROOMS_REQUEST_CODE:
            result = getAvailableRooms(request);
            break;
        case GET_PLAYERS_REQUEST_CODE:
            result = getPlayersInRoom(request);
            break;
        case GET_STATISTICS_REQUEST_CODE:
            result = getStatistics(request);
            break;
        case JOIN_ROOM_REQUEST_CODE:
            result = joinRoom(request);
            break;
        case CREATE_ROOM_REQUEST_CODE:
            result = createRoom(request);
            break;
        default:
            throw std::exception(__FUNCTION__);
    }
    return result;
}

/*
This function handles the loss of connection with the client.
Input: None.
Output: None.
*/
void MenuRequestHandler::connectionLost()
{
    m_handlerFactory.getLoginManager().logout(m_user.getUsername());
}


/*
This function logs the user out of the server.
Input: request - The "signout" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult MenuRequestHandler::signout(RequestInfo request)
{
    RequestResult result;
    LogoutResponse response;
    try
    {
        m_handlerFactory.getLoginManager().logout(m_user.getUsername());
        response.status = SUCCESS_CODE;
        result.response = JsonResponsePacketSerializer::serializeResponse(response);
        result.newHandler = m_handlerFactory.createLoginRequestHandler();
    }
    catch (const std::exception& e)
    {
        ErrorResponse errorResponse;
        errorResponse.message = "ERROR";
        result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = nullptr;
    }
    return result;
}

/*
This function returns all of the available rooms.
Input: request - The "getRooms" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult MenuRequestHandler::getAvailableRooms(RequestInfo request)
{
    RequestResult result;
    GetRoomsResponse response;
    try
    {
        response.rooms = m_roomManager.getAvailableRooms();
        response.status = SUCCESS_CODE;
        result.response = JsonResponsePacketSerializer::serializeResponse(response);
    }
    catch (const std::exception& e)
    {
        ErrorResponse errorResponse;
        errorResponse.message = e.what();
        result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
    }
    result.newHandler = nullptr;
    return result;
}

/*
This function returns all of the users in a room.
Input: request - The "getPlayersInRoom" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo request)
{
    RequestResult result;
    GetPlayersInRoomResponse response;
    GetPlayersInRoomRequest getPlayersRequest = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(request.buffer);
    try
    {
        response.players = m_roomManager.getPlayersInRoom(getPlayersRequest.roomId);
        response.status = SUCCESS_CODE;
        result.response = JsonResponsePacketSerializer::serializeResponse(response);
    }
    catch (const std::exception& e)
    {
        ErrorResponse errorResponse;
        errorResponse.message = e.what();
        result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
    }
    result.newHandler = nullptr;
    return result;
}

/*
This function returns the statistics of the game.
Input: request - The "getStatistics" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult MenuRequestHandler::getStatistics(RequestInfo request)
{
    //The result that will be returned from the function.
    RequestResult result;
    GetStatisticsResponse response;
    try
    {
        response.highScores = m_statisticsManager.getHighScore();
        response.userStatistics = m_statisticsManager.getUserStatistics(m_user.getUsername());
        response.status = SUCCESS_CODE;
        result.response = JsonResponsePacketSerializer::serializeResponse(response);
    }
    catch (const std::exception& e)
    {
        ErrorResponse errorResponse;
        errorResponse.message = e.what();
        result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
    }
    result.newHandler = nullptr;
    return result;
}

/*
This function adds the user to a room.
Input: request - The "joinRoom" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult MenuRequestHandler::joinRoom(RequestInfo request)
{
    RequestResult result;
    JoinRoomResponse response;
    JoinRoomRequest joinRoomRequest = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(request.buffer);
    try
    {
        m_roomManager.addUserToRoom(joinRoomRequest.roomId, m_user);
        response.status = SUCCESS_CODE;
        result.response = JsonResponsePacketSerializer::serializeResponse(response);
        auto rooms = m_roomManager.getRooms();
        result.newHandler = m_handlerFactory.createRoomMemberRequestHandler(m_user, *std::find(rooms.begin(), rooms.end(), joinRoomRequest.roomId));
    }
    catch (const std::exception& e)
    {
        ErrorResponse errorResponse;
        errorResponse.message = e.what();
        result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = nullptr;
    }
    return result;
}

/*
This function creates a new room.
Input: request - The "createRoom" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult MenuRequestHandler::createRoom(RequestInfo request)
{
    RequestResult result;
    CreateRoomResponse response;
    CreateRoomRequest createRoomRequest = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(request.buffer);
    RoomData roomSettings = { m_roomManager.getNewRoomId(), createRoomRequest.roomName, createRoomRequest.maxUsers, createRoomRequest.questionCount, createRoomRequest.answerTimeout, false };
    try
    {
        unsigned int maxNumQuestions = getMaxQuestions();
        if (createRoomRequest.questionCount > maxNumQuestions) throw std::exception(("You can only have up to " + std::to_string(maxNumQuestions) + " questions in your game.").c_str());
        m_roomManager.createRoom(m_user, roomSettings);
        response.status = SUCCESS_CODE;
        response.roomId = roomSettings.id;
        result.response = JsonResponsePacketSerializer::serializeResponse(response);
        result.newHandler = m_handlerFactory.createRoomAdminRequestHandler(m_user, Room(roomSettings));
    }
    catch (const std::exception& e)
    {
        ErrorResponse errorResponse;
        errorResponse.message = e.what();
        result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
        result.newHandler = nullptr;
    }
    return result;
}

/*
* Get the maximum number of questions the server has
*/
unsigned int MenuRequestHandler::getMaxQuestions()
{
    return m_database->getMaxNumOfQuestions();
}
