#include "RoomMemberRequestHandler.h"

/*
A constructor function. This function creates a new "RoomMemberRequestHandler" type object.
Input: roomManager - The room manager that will be used by the handler to manage rooms.
	   requestHandlerFactory - The request handler factory that will be used by the handler to create new handles.
	   user - The user.
	   room - The room the user is in.
Output: None.
*/
RoomMemberRequestHandler::RoomMemberRequestHandler(RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory, LoggedUser user, Room room) :
	m_roomManager(roomManager), m_handlerFactory(requestHandlerFactory), m_user(user), m_room(room)
{
}

/*
This function checks whether or not a request is relevant.
Input: request - The request that will be checked.
Output: boolean - Whether or not the request is relevant.
*/
bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo& request) const
{
	return request.requestId == GET_ROOM_STATE_REQUEST_CODE || request.requestId == LEAVE_ROOM_REQUEST_CODE;
}

/*
This function handles the request based on its content.
Input: request - The request that was sent by the user.
Output: The result of the request (response to the client + next handle of the socket).
*/
RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo& request)
{
	RequestResult result;
	switch (request.requestId)
	{
	case LEAVE_ROOM_REQUEST_CODE:
		result = leaveRoom(request);
		break;
	case GET_ROOM_STATE_REQUEST_CODE:
		result = getRoomState(request);
		break;
	default:
		throw std::exception(__FUNCTION__);
		break;
	}
	return result;
}

/*
This function handles the loss of connection with the client.
Input: None.
Output: None.
*/
void RoomMemberRequestHandler::connectionLost()
{
	m_roomManager.removeUserFromRoom(m_room.getRoomData().id, m_user);
	m_handlerFactory.getLoginManager().logout(m_user.getUsername());
}

/*
This function disconnects the user from the room.
Input: request - The "leaveRoom" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo requestInfo)
{
	RequestResult result;
	try
	{
		LeaveRoomResponse response;
		m_roomManager.removeUserFromRoom(m_room.getRoomData().id, m_user);
		response.status = SUCCESS_CODE;
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
		result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	}
	catch (const std::out_of_range e)
	{
		ErrorResponse errorResponse;
		errorResponse.message = "The room has been closed.";
		result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
	}
	catch (const std::exception& e)
	{
		ErrorResponse errorResponse;
		errorResponse.message = e.what();
		result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		result.newHandler = nullptr;
	}
	return result;
}

/*
This function gets the room state.
Input: request - The "getRoomState" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo requestInfo)
{
	RequestResult result;
	result.newHandler = nullptr;
	try
	{
		GetRoomStateResponse response;
		response.hasGameBegun = m_roomManager.getRoomState(m_room.getRoomData().id);
		response.answerTimeout = m_room.getRoomData().timePerQuestion;
		response.players = m_roomManager.getPlayersInRoom(m_room.getRoomData().id);
		response.answerCount = m_room.getRoomData().numOfQusetionInGame;
		response.status = SUCCESS_CODE;
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
		if (response.hasGameBegun)
		{
			result.newHandler = m_handlerFactory.createGameRequestHandler(m_handlerFactory.getGameManager().joinGame(m_user, m_room.getRoomData()), m_user);
		}
	}
	catch (const std::out_of_range e)
	{
		ErrorResponse errorResponse;
		errorResponse.message = "The room has been closed.";
		result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	}
	catch (const std::exception& e)
	{
		ErrorResponse errorResponse;
		errorResponse.message = e.what();
		result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
	}
	return result;
}
