#include "Communicator.h"
#include <iostream>
#include <thread>
#include "LoginRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include <ctime>

/*
* Creates the socket of the server.
* Input: None.
* Output: None.
*/
Communicator::Communicator(RequestHandlerFactory& handlerFactory)
	: m_handlerFactory(handlerFactory)
{
	std::cout << "Starting..." << std::endl;
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

/*
* Frees the resources that were allocated in the constructor.
* Input: None.
* Output: None.
*/
Communicator::~Communicator()
{
	try
	{
		closesocket(m_serverSocket);
	}
	catch (...) {}
}

/*
* Public function to start server operation
* Input: None.
* Output: None.
*/
void Communicator::startHandleRequests()
{
	bindAndListen();
}

/*
* Binds the server listening socket and listens to clients.
* Input: None.
* Output: None.
*/
void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(LISTENING_PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration (port and etc..)
	if (bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	std::cout << "binded" << std::endl;

	// Start listening for incoming requests of clients
	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << LISTENING_PORT << std::endl;
	std::cout << "accepting client..." << std::endl;

	while (true)
	{
		accept();
	}
}

/*
* This function accepets a new clients and passes him to a new thread.
* Input: None.
* Output: None.
*/
void Communicator::accept()
{
	//Accepts the client and create a specific socket from server to this client.
	SOCKET clientSocket = ::accept(m_serverSocket, NULL, NULL);

	if (clientSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	std::cout << "Client accepted on port " << clientSocket << " !" << std::endl;

	//Add to map.
	IRequestHandler* requestHandler = m_handlerFactory.createLoginRequestHandler();
	m_clients.insert(std::pair<SOCKET, IRequestHandler*>(clientSocket, requestHandler));

	//The function that handles the conversation with the client.
	std::thread t1(&Communicator::handleNewClient, this, clientSocket);
	t1.detach();
}

/*
* Handles the client
* Input: clientSocket - conversation socket.
* Output: None.
*/
void Communicator::handleNewClient(SOCKET clientSocket)
{
	RequestInfo request; //The request from the client.
	RequestResult result; //The result from the handler.
	std::string username; //The name of the user.
	try
	{
		do
		{
			request = gatherRequestInfo(clientSocket);
			auto requestHandler = m_clients.at(clientSocket);
			if (requestHandler->isRequestRelevant(request))
			{
				result = requestHandler->handleRequest(request);
				if (result.newHandler != nullptr)
				{
					delete m_clients[clientSocket];
					m_clients[clientSocket] = result.newHandler;
				}
				sendBin(clientSocket, result.response);
			}
			else
			{
				//Error response.
				sendBin(clientSocket, JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ "Unsupported action." }));
			}
		} while (true);
	}
	catch (const std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
		m_clients.at(clientSocket)->connectionLost();
	}
	std::cout << "Conversation with client " + username + " on socket " + std::to_string(clientSocket) + " has ended" << std::endl;
	if (result.newHandler != nullptr) delete m_clients[clientSocket];
	m_clients.erase(clientSocket); //Removes the socket from the map.
	closesocket(clientSocket);
}

/*
This function gathers the info regarding the request info, then returns the complete RequestInfo struct.
Input: clietnSocket - The socket with the user.
Output: The RequestInfo that stores the data regarding the user's request.
*/
RequestInfo Communicator::gatherRequestInfo(SOCKET clientSocket)
{
	RequestInfo request;
	request.requestId = JsonRequestPacketDeserializer::bin1byteToInt(getBinFromSocket(clientSocket, REQUEST_ID_BYTES_NUM));
	int messageSize = JsonRequestPacketDeserializer::bin4byteToInt(getBinFromSocket(clientSocket, MESSAGE_BYTES_NUM));
	request.buffer = getBinFromSocket(clientSocket, messageSize); //Throws "bad allocation exception" if the socket disconnects.
	request.receivalTime = std::time(0);
	return request;
}

/*
* Gets bytes message from the client.
* Input: sc - Clients socket. 
		 bytesNum - Message size.
* Output: The message (vector<unsigned char>).
*/
std::vector<unsigned char> Communicator::getBinFromSocket(SOCKET sc, int bytesNum)
{
	if (bytesNum == 0)
	{
		return std::vector<unsigned char>();
	}

	char* data = new char[bytesNum];
	int res = recv(sc, data, bytesNum, 0);

	if (res == INVALID_SOCKET)
	{
		delete[] data;
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	std::vector<unsigned char> buffer(data, data + bytesNum);
	delete[] data;
	return buffer;
}


/*
* Sends bytes message to client.
* Input: sc - clients socket
		 buffer - the message (vector<unsigned char>).
* Output: None.
*/
void Communicator::sendBin(SOCKET sc, std::vector<unsigned char> buffer)
{
	char* data = new char[buffer.size()];
	std::copy(buffer.begin(), buffer.end(), data);

	if (send(sc, data, buffer.size(), 0) == INVALID_SOCKET)
	{
		delete[] data;
		throw std::exception("Error while sending the message to client.");
	}
	delete[] data;
}
