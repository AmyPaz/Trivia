#include "LoginManager.h"

/*
A constructor function. This function creates a new "LoginManager" type object.
Input: None.
Output: None.
*/
LoginManager::LoginManager(IDatabase* database) :
	m_database(database)
{
	try
	{
		m_database->openDB();
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
}

/*
A destructor function. This function frees the memory allocated by a "LoginManager" type object.
Input: None.
Output: None.
*/
LoginManager::~LoginManager()
{
}

/*
This function signs up a new user in the system.
Input: username - The user's username.
	   password - The user's password.
	   email - The user's email address.
Output: None.
*/
void LoginManager::signup(std::string username, std::string password, std::string email) const
{
	if (m_database->doesUserExist(username))
	{
		throw std::exception("Username taken.");
	}
	m_database->addNewUser(username, password, email);
}

/*
This function logs in a registered user.
Input: username - The user's username.
	   password - The user's password.
Output: None.
*/
void LoginManager::login(std::string username, std::string password)
{
	if (!m_database->doesUserExist(username))
	{
		throw std::exception("User does not exist.");
	}

	//Searches for the user in the list of logged users.
	if (std::find(m_loggedUsers.begin(), m_loggedUsers.end(), LoggedUser(username)) != m_loggedUsers.end()) 
	{
		throw std::exception("This user is already logged in.");
	}

	
	if (m_database->doesPasswordMatch(username, password))
	{
		std::lock_guard<std::mutex> lock(_usersMutex);
		m_loggedUsers.push_back(LoggedUser(username));
	}

	else
	{
		throw std::exception("Incorrect password.");
	}
}

/*
This function dissconnects a connected user from the server.
Input: username - The user's username.
Output: None.
*/
void LoginManager::logout(std::string username)
{
	auto user = std::find(m_loggedUsers.begin(), m_loggedUsers.end(), LoggedUser(username)); //Searches for the user in the list of logged users.
	if (user == m_loggedUsers.end())
	{
		throw std::exception("User is not logged in!");
	}
	else
	{
		std::lock_guard<std::mutex> lock(_usersMutex);
		m_loggedUsers.erase(user);
	}
}