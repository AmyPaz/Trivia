#pragma once
#include "LoginRequestHandler.h"
#include "IDatabase.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "GameManager.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"


class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;
class RequestHandlerFactory
{
public:
	RequestHandlerFactory(IDatabase* database);
	~RequestHandlerFactory();
	LoginManager& getLoginManager();
	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(LoggedUser l);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser l, Room room);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser l, Room room);
	StatisticsManager& getStatisticsManager();
	RoomManager& getRoomManager();
	GameRequestHandler* createGameRequestHandler(Game game, LoggedUser user);
	GameManager& getGameManager();

private:
	IDatabase* m_database;
	LoginManager m_loginManager;
	RoomManager m_roomManager;
	GameManager m_gameManager;
	StatisticsManager m_StatisticsManager;
};

