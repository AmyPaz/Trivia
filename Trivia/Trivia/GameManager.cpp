#include "GameManager.h"

/*
A constructor function. This function creates a new "GameManager" type object.
Input: database - A pointer to the database manager.
Output: None.
*/
GameManager::GameManager(IDatabase* database) :
	m_database(database)
{
}

/*
This function creates a new game and adds it to the map of games.
Input: users - The users from the room that the game is based of.
	   roomData - The data from the room that the game is based of.
Output: The new game that was created.
*/
Game GameManager::createGame(std::vector<LoggedUser> users, RoomData roomData)
{
	Game game = Game(users, m_database->getQuestions(roomData.numOfQusetionInGame), roomData.id, roomData.timePerQuestion);
	std::lock_guard<std::mutex> lock(_gamesMutex);
	m_games.insert({ roomData.id, game });
	return game;
}

/*
This function creates a new game and adds it to the map of games.
Input: user - The user from the room that the game is based of.
	   roomData - The data from the room that the game is based of.
Output: The game that was joind.
*/
Game GameManager::joinGame(LoggedUser user, RoomData roomData)
{
	std::lock_guard<std::mutex> lock(_gamesMutex);
	m_games.at(roomData.id).addPlayer(user);
	return m_games.at(roomData.id);
}

/*
This function removes a game from the map of games.
Input: id - The id of the room that will be deleted.
Output: None.
*/
void GameManager::deleteGame(unsigned int id)
{
	std::lock_guard<std::mutex> lock(_gamesMutex);
	m_games.erase(id);
}

/*
This function returns the question that the user has to answer next.
Input: gameId - The ID of the user's game.
	   user - The user who will receive the question.
Output: The next question that the user has to answer.
*/
Question GameManager::getQuestionForUser(unsigned int gameId, LoggedUser user)
{
	std::lock_guard<std::mutex> lock(_gamesMutex);
	return m_games.at(gameId).getQuestionForUser(user);
}

/*
This function submits the user's answer.
Input: gameId - The ID of the user's game.
	   user - The user who submits the answer.
	   answer - The user's answer.
	   timeToAnswer - The time it took the player to answer the question.
Output: Whether or not the user's answer was correct.
*/
void GameManager::submitAnswer(unsigned int gameId, LoggedUser user, unsigned int answer, unsigned int timeToAnswer)
{
	int points = 0;
	std::lock_guard<std::mutex> lock(_gamesMutex);
	bool answeredCorrectly = m_games.at(gameId).submitAnswer(user, answer, timeToAnswer);
	unsigned int answerTimeout = m_games.at(gameId).getAnswerTimeout();
	if (timeToAnswer < answerTimeout)
	{
		points = answeredCorrectly * ((answerTimeout - timeToAnswer) / ((double)answerTimeout) * POINTS_MULTIPLIER);
	}
	else
	{
		timeToAnswer = answerTimeout;
	}
	m_database->submitAnswer(user.getUsername(), gameId, timeToAnswer, answeredCorrectly, points);
	m_games.at(gameId).addPoints(user, points);

	
}

/*
This function is used if a player leaves the game in the middle
Input: gameId - The ID of the user's game.
	   user - The user who will no longer participate in the game.
Output: None.
*/
void GameManager::playerLeft(unsigned int gameId, LoggedUser user)
{
	std::lock_guard<std::mutex> lock(_gamesMutex);
	m_games.at(gameId).playerLeft(user);
}

/*
This function returns the results of the game.
Input: gameId - The ID of the game whose results will be returned.
Output: A vector that contains the results of the game.
*/
std::vector<PlayerResult> GameManager::getGameResults(unsigned int gameId)
{
	std::lock_guard<std::mutex> lock(_gamesMutex);
	return m_games.at(gameId).getResults();
}
