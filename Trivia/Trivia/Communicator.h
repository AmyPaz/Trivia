#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include <string>
#include <vector>

#define LISTENING_PORT 8853

//The size of the "message length" field in the message.
#define MESSAGE_BYTES_NUM 4

//The size of the "request id" field in the message.
#define REQUEST_ID_BYTES_NUM 1

class Communicator
{
public:
	Communicator(RequestHandlerFactory& handlerFactory);
	~Communicator();
	void startHandleRequests();

private:
	void bindAndListen();
	void accept();
	void handleNewClient(SOCKET clientSocket);
	RequestInfo gatherRequestInfo(SOCKET clientSocket);
	std::vector<unsigned char> getBinFromSocket(SOCKET sc, int bytesNum);
	void sendBin(SOCKET sc, std::vector<unsigned char> buffer);

	SOCKET m_serverSocket;
	std::map <SOCKET, IRequestHandler*> m_clients;
	RequestHandlerFactory& m_handlerFactory;
};

