#pragma once
#include <iostream>

class LoggedUser
{
public:
	LoggedUser(const std::string& username);
	std::string getUsername() const;
	bool operator==(const LoggedUser& lg) const;
	bool operator<(const LoggedUser& lg) const;

private:
	std::string m_username; //The username of the logged user.
};

