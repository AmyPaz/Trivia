#pragma once
#include "SqliteDatabase.h"
#include "room.h"
#include "Game.h"

#define POINTS_MULTIPLIER 100 

class GameManager
{
public:
	GameManager(IDatabase* database);
	~GameManager() = default;
	Game createGame(std::vector<LoggedUser> users, RoomData roomData);
	Game joinGame(LoggedUser users, RoomData roomData);
	void deleteGame(unsigned int id);
	Question getQuestionForUser(unsigned int gameId, LoggedUser user);
	void submitAnswer(unsigned int gameId, LoggedUser user, unsigned int answer, unsigned int timeToAnswer);
	void playerLeft(unsigned int gameId, LoggedUser user);
	std::vector<PlayerResult> getGameResults(unsigned int gameId);
private:
	IDatabase* m_database; //The database that is used by the program.
	std::map<unsigned int, Game> m_games;
	std::mutex _gamesMutex; //Used to block users from accessing the games list when it is used by another user.
};

