#include "Question.h"

/*
A constructor function. This function creates a new "Question" type object.
Input: question - The question itself.
	   possibleAnswers - Four possible answers to the questions.
Output: None.
*/
Question::Question(std::string question, std::vector<std::string> possibleAnswers) :
	m_question(question), m_possibleAnswers(possibleAnswers)
{
}

/*
A getter function. This function returns the question itself.
Input: None.
Output: The question itself.
*/
std::string Question::getQuestion()
{
	return this->m_question;
}

/*
A getter function. This function returns the four possible answers to the question.
Input: None.
Output: The four possible answers to the question.
*/
std::vector< std::string> Question::getPossibleAnswers()
{
	return m_possibleAnswers;
}

/*
A getter function. This function returns the correct answer to the question.
Input: None.
Output: The correct answer to the question.
*/
std::string Question::getCorrentAnswer()
{
	return this->m_possibleAnswers[CORRECT_ANSWER_INDEX];
}
