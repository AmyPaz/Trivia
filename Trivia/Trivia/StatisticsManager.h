#pragma once
#include "IDatabase.h"
#include <iostream>
#include <vector>
#include <string>
#include <map>


class StatisticsManager
{
public:
	StatisticsManager(IDatabase* database);
	std::map<std::string, int> getHighScore();
	std::map<std::string, int> getUserStatistics(std::string username);

private:
	IDatabase* m_database; //Access to the database.
};

