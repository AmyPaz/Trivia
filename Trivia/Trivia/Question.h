#pragma once
#include <iostream>
#include <vector>

#define ANSWERS_DELIM "," //Used to separate the question in the function "getPossibleAnswers".
#define CORRECT_ANSWER_INDEX 0 //The index of the correct answer in the vector "m_possibleAnswers".

class Question
{
public:
	Question(std::string question, std::vector<std::string> possibleAnswers);
	std::string getQuestion();
	std::vector<std::string> getPossibleAnswers();
	std::string getCorrentAnswer();

private:
	std::string m_question;
	std::vector<std::string> m_possibleAnswers;
};

