#include "room.h"

/*
* constructor for game room
* Input: room data
* Output: none
*/
Room::Room(RoomData data)
{
	m_roomData = data;
}

/*
* add user to game room
* Input: user to add
* Output: none
*/
void Room::addUser(LoggedUser user)
{
	m_users.push_back(user);
}

/*
* remove user from game room
* Input: user to remove
* Output: none
*/
void Room::removeUser(LoggedUser user)
{
	auto it = std::find(m_users.begin(), m_users.end(), user);
	if (it != m_users.end()) m_users.erase(it);
}

/*
* get all users from game room
* Input: none
* Output: vector of all user names
*/
std::vector<std::string> Room::getAllUsers()
{
	std::vector<std::string> strUsers;
	for (LoggedUser user : m_users) strUsers.push_back(user.getUsername());
	return strUsers;
}

/*
This function returns the number of users in the room.
Input: None.
Output: The number of players in the room.
*/
unsigned int Room::getNumberOfUsers()
{
	return m_users.size();
}

/*
* get room data
* Input: none
* Output: room data
*/
RoomData Room::getRoomData()
{
	return m_roomData;
}

/*
A == operator for the object. Check is the current room is the same as another room.
Input: id - The id of the other room.
Output: Whether or not the rooms are the same.
*/
bool Room::operator==(const unsigned int& id) const
{
	return m_roomData.id == id;
}

/*
This function is a setter function for the attribute "isActive" of a room.
Input: state - Whether or not the room is active.
Output: None.
*/
void Room::setIsActive(unsigned int state)
{
	this->m_roomData.isActive = state;
}
