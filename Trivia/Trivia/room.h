#pragma once
#include <string>
#include <vector>
#include "LoggedUser.h"

typedef struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int numOfQusetionInGame;
	unsigned int timePerQuestion;
	unsigned int isActive;
	bool operator ==(const unsigned int& id) const
	{
		return id == this->id;
	}
} RoomData;

class Room
{
public:
	Room(RoomData data);
	void addUser(LoggedUser user);
	void removeUser(LoggedUser user);
	std::vector<std::string> getAllUsers();
	unsigned int getNumberOfUsers();
	RoomData getRoomData();
	bool operator ==(const unsigned int& id) const;
	void setIsActive(unsigned int state);
private:
	RoomData m_roomData;
	std::vector<LoggedUser> m_users;
};

