#include "RequestHandlerFactory.h"

/*
A constructor function. This function creates a new "RequestHandlerFactory" type object.
Input: database - The database that the program uses.
Output: None.
*/
RequestHandlerFactory::RequestHandlerFactory(IDatabase* database) :
    m_database(database), m_loginManager(database), m_StatisticsManager(database), m_gameManager(database)
{
}

/*
A destructor function. This function frees the memory that was allocated by the constructor.
Input: None.
Output: None.
*/
RequestHandlerFactory::~RequestHandlerFactory()
{
}

/*
This function creates a LoginRequestHandler.
Input: None.
Output: A LoginRequestHandler.
*/
LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
    return new LoginRequestHandler(*this);
}

/*
A getter function. This function returns the LoginManager of the RequestHandlerFactory.
Input: None.
Output: A reference to the LoginManager of the RequestHandlerFactory.
*/
LoginManager& RequestHandlerFactory::getLoginManager()
{
    return m_loginManager;
}

/*
This function creates a new MenuRequestHandler.
Input: l - The user.
Output: A new MenuRequestHandler.
*/
MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser l)
{
    return new MenuRequestHandler(m_roomManager, m_StatisticsManager, *this, l, m_database);
}

/*
This function creates a new RoomAdminRequestHandler.
Input: l - The user.
       room - The admin's room.
Output: A new RoomAdminRequestHandler.
*/
RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser l, Room room)
{
    return new RoomAdminRequestHandler(m_roomManager, *this, l, room);
}

/*
This function creates a new RoomMemberRequestHandler.
Input: l - The user.
       room - The user's room.
Output: A new RoomAdminRequestHandler.
*/
RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser l, Room room)
{
    return new RoomMemberRequestHandler(m_roomManager, *this, l, room);
}

/*
A getter function. This function returns the StatisticsManager of the RequestHandlerFactory.
Input: None.
Output: A reference to the StatisticsManager of the RequestHandlerFactory.
*/
StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
    return m_StatisticsManager;
}

/*
A getter function. This function returns the RoomManager of the RequestHandlerFactory.
Input: None.
Output: A reference to the RoomManager of the RequestHandlerFactory.
*/
RoomManager& RequestHandlerFactory::getRoomManager()
{
    return m_roomManager;
}

/*
This function creates a new GameRequestHandler.
Input: None.
Output: A new GameRequestHandler.
*/
GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(Game game, LoggedUser user)
{
    return new GameRequestHandler(game, user, m_gameManager, *this);
}

/*
A getter function. This function returns the GameManager of the RequestHandlerFactory.
Input: None.
Output : A reference to the GameManager of the RequestHandlerFactory.
*/
GameManager& RequestHandlerFactory::getGameManager()
{
    return m_gameManager;
}
