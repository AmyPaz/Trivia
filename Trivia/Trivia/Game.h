#pragma once
#include "Question.h"
#include "LoggedUser.h"
#include <map>
#include <set>

typedef struct GameData
{
	Question currentQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	float averageAnswerTime;
	unsigned int pointsScored;
} GameData;

typedef struct PlayerResult
{
	std::string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	float averageAnswerTime;
	unsigned int pointsScored;

	bool operator<(const PlayerResult& other) const
	{
		if (other.pointsScored != this->pointsScored)
		{
			return this->pointsScored < other.pointsScored;
		}
		else if (other.correctAnswerCount != this->correctAnswerCount)
		{
			return this->correctAnswerCount < other.correctAnswerCount;
		}
		return this->averageAnswerTime < other.averageAnswerTime;
	}
} PlayerResult;


class Game
{
public:
	Game(std::vector<LoggedUser> users, std::vector<Question> questions, unsigned int id, unsigned int answerTimeout);
	Question getQuestionForUser(LoggedUser user);
	bool submitAnswer(LoggedUser user, unsigned int answer, unsigned int timeToAnswer);
	void addPoints(LoggedUser user, int points);
	void addPlayer(LoggedUser user);
	void playerLeft(LoggedUser user);
	bool operator ==(const Game& game) const;
	int getGameId() const;
	unsigned int getAnswerTimeout() const;
	std::vector<PlayerResult> getResults() const;
private:
	unsigned int m_id;
	unsigned int m_answerTimeout;
	std::vector<Question> m_questions;
	std::map<LoggedUser, GameData> m_players;
};