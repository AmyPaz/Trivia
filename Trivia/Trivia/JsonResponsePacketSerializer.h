#pragma once
#include <string>
#include <vector>
#include <sstream>
#include "json.hpp"
#include "room.h"
#include "Game.h"
#include "Question.h"

using json = nlohmann::json;

//The size of a byte in bits.
#define BYTE_SIZE 8

//The size of the "message length" field in the buffer that the user sends.
#define MESSAGE_BYTES_NUM 4

//The number of user related statistics.
#define NUMBER_OF_USER_STATISTICS 4

//Used to seperate the key from the value of the different statistics.
#define STATISTICS_DELIM ":"

enum responseCodes {ERROR_CODE = 254, SUCCESS_CODE};

typedef struct ErrorResponse
{
	std::string message;
}ErrorResponse;


typedef struct LoginResponse
{
	unsigned int status;
}LoginResponse;


typedef struct SignupResponse
{
	unsigned int status;
}SignupResponse;

typedef struct LogoutResponse
{
	unsigned int status;
} LogoutResponse;

typedef struct GetRoomsResponse
{
	unsigned int status;
	std::vector<RoomData> rooms;
} GetRoomsResponse;

typedef struct GetPlayersInRoomResponse
{
	unsigned int status;
	std::vector<std::string> players;
} GetPlayersInRoomResponse;

typedef struct GetStatisticsResponse
{
	unsigned int status;
	std::map<std::string, int> highScores;
	std::map<std::string, int> userStatistics;
} getStatisticsResponse;

typedef struct JoinRoomResponse
{
	unsigned int status;
} JoinRoomResponse;

typedef struct CreateRoomResponse
{
	unsigned int status;
	unsigned int roomId;
} CreateRoomResponse;

typedef struct CloseRoomResponse
{
	unsigned int status;
} CloseRoomResponse;

typedef struct LeaveRoomResponse
{
	unsigned int status;
} LeaveRoomResponse;

typedef struct StartGameResponse
{
	unsigned int status;
} StartGameResponse;

typedef struct GetRoomStateResponse
{
	unsigned int status;
	bool hasGameBegun;
	std::vector<std::string> players;
	unsigned int answerCount;
	unsigned int answerTimeout;
} GetRoomStateResponse;

typedef struct GetGameResultsResponse
{
	unsigned int status;
	std::vector<PlayerResult> results;
} GetGameResultsResponse;

typedef struct SubmitAnswerResponse
{
	unsigned int status;
	unsigned int correctAnswerId;
} SubmitAnswerResponse;

typedef struct LeaveGameResponse
{
	unsigned int status;
} LeaveGameResponse;

typedef struct GetQuestionResponse
{
	unsigned int status;
	std::string question;
	std::vector<std::string> answers;
} GetQuestionResponse;

class JsonResponsePacketSerializer
{
public:
	static std::vector<unsigned char> serializeResponse(ErrorResponse s);
	static std::vector<unsigned char> serializeResponse(LoginResponse s);
	static std::vector<unsigned char> serializeResponse(SignupResponse s);
	static std::vector<unsigned char> serializeResponse(LogoutResponse s);
	static std::vector<unsigned char> serializeResponse(GetRoomsResponse s);
	static std::vector<unsigned char> serializeResponse(GetPlayersInRoomResponse s);
	static std::vector<unsigned char> serializeResponse(JoinRoomResponse s);
	static std::vector<unsigned char> serializeResponse(CreateRoomResponse s);
	static std::vector<unsigned char> serializeResponse(GetStatisticsResponse s);
	static std::vector<unsigned char> serializeResponse(CloseRoomResponse s);
	static std::vector<unsigned char> serializeResponse(LeaveRoomResponse s);
	static std::vector<unsigned char> serializeResponse(StartGameResponse s);
	static std::vector<unsigned char> serializeResponse(GetRoomStateResponse s);
	static std::vector<unsigned char> serializeResponse(GetGameResultsResponse s);
	static std::vector<unsigned char> serializeResponse(SubmitAnswerResponse s);
	static std::vector<unsigned char> serializeResponse(GetQuestionResponse s);
	static std::vector<unsigned char> serializeResponse(LeaveGameResponse s);
private:
	static std::vector<unsigned char> intTo4ByteBin(const int num);
	static std::vector<unsigned char> intToByteBin(const int num);
	static std::vector<unsigned char> createResponse(json j, int code);
};
