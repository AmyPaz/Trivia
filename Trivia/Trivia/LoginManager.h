#pragma once
#include "LoggedUser.h"
#include "SqliteDatabase.h"
#include <vector>
#include <algorithm>
#include <mutex>

class LoginManager
{
public:
	LoginManager(IDatabase* database);
	~LoginManager();
	void signup(std::string username, std::string password, std::string email) const;
	void login(std::string username, std::string password);
	void logout(std::string username);

private:
	IDatabase* m_database; //The database that is used by the program.
	std::vector<LoggedUser> m_loggedUsers;
	std::mutex _usersMutex; //Used to block users from accessing the users list when it is used by another user.
};

