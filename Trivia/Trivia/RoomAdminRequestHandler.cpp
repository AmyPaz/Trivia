#include "RoomAdminRequestHandler.h"

/*
A constructor function. This function creates a new "RoomAdminRequestHandler" type object.
Input: roomManager - The room manager that will be used by the handler to manage rooms.
	   requestHandlerFactory - The request handler factory that will be used by the handler to create new handles.
	   user - The user.
	   room - The room the user is in.
Output: None.
*/
RoomAdminRequestHandler::RoomAdminRequestHandler(RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory, LoggedUser user, Room room) :
	m_roomManager(roomManager), m_handlerFactory(requestHandlerFactory), m_user(user), m_room(room)
{
}

/*
This function checks whether or not a request is relevant.
Input: request - The request that will be checked.
Output: boolean - Whether or not the request is relevant.
*/
bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo& request) const
{
	return request.requestId == CLOSE_ROOM_REQUEST_CODE ||
		request.requestId == START_GAME_REQUEST_CODE || request.requestId == GET_ROOM_STATE_REQUEST_CODE;
}

/*
This function handles the request based on its content.
Input: request - The request that was sent by the user.
Output: The result of the request (response to the client + next handle of the socket).
*/
RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo& request)
{
	RequestResult result;
	switch (request.requestId)
	{
	case CLOSE_ROOM_REQUEST_CODE:
		result = closeRoom(request);
		break;
	case START_GAME_REQUEST_CODE:
		result = startGame(request);
		break;
	case GET_ROOM_STATE_REQUEST_CODE:
		result = getRoomState(request);
		break;
	default:
		throw std::exception(__FUNCTION__);
		break;
	}
	return result;
}

/*
This function handles the loss of connection with the client.
Input: None.
Output: None.
*/
void RoomAdminRequestHandler::connectionLost()
{
	m_roomManager.deleteRoom(m_room.getRoomData().id);
	m_handlerFactory.getLoginManager().logout(m_user.getUsername());
}

/*
This function closes the room.
Input: request - The "closeRoom" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo requestInfo)
{
	RequestResult result;
	try
	{
		CloseRoomResponse response;
		m_roomManager.deleteRoom(m_room.getRoomData().id);
		response.status = SUCCESS_CODE;
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
		result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	}
	catch (const std::exception& e)
	{
		ErrorResponse errorResponse;
		errorResponse.message = "Failed to close the room.";
		result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		result.newHandler = nullptr;
	}
	return result;
}

/*
This function starts the game.
Input: request - The "startGame" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult RoomAdminRequestHandler::startGame(RequestInfo requestInfo)
{
	RequestResult result;
	try
	{
		StartGameResponse response;
		m_roomManager.setRoomState(m_room.getRoomData().id, true);
		response.status = SUCCESS_CODE;
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
		std::vector<LoggedUser> users;
		for (std::string name : m_roomManager.getPlayersInRoom(m_room.getRoomData().id)) users.push_back(LoggedUser(name));
		result.newHandler = m_handlerFactory.createGameRequestHandler(m_handlerFactory.getGameManager().createGame(users, m_room.getRoomData()), m_user); 
	}
	catch (const std::exception& e)
	{
		ErrorResponse errorResponse;
		errorResponse.message = e.what();
		result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
		result.newHandler = nullptr;
	}
	return result;
}

/*
This function gets the room state.
Input: request - The "getRoomState" request that was sent by the user.
Output: RequestReuslt (The response from the server + the next handler to be used).
*/
RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo requestInfo)
{
	RequestResult result;
	try
	{
		GetRoomStateResponse response;
		response.hasGameBegun = m_roomManager.getRoomState(m_room.getRoomData().id);
		response.answerTimeout = m_room.getRoomData().timePerQuestion;
		response.players = m_roomManager.getPlayersInRoom(m_room.getRoomData().id);
		response.answerCount = m_room.getRoomData().numOfQusetionInGame;
		response.status = SUCCESS_CODE;
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
	}
	catch (const std::exception& e)
	{
		ErrorResponse errorResponse;
		errorResponse.message = e.what();
		result.response = JsonResponsePacketSerializer::serializeResponse(errorResponse);
	}
	result.newHandler = nullptr;
	return result;
}
	
