#pragma once
#include <map>
#include <vector>
#include <mutex>
#include "Room.h"

#define MIN_NUM_PLAYERS 1 //The minimum number of players that is required for a room to be created.
#define MIN_NUM_QUSTIONS 1 //The minimum number of questions that is required for a room to be created.
#define MIN_TIME_QUSTIONS 3 //The minimum number of time per question that is required for a room to be created.

class RoomManager
{
public:
	void createRoom(LoggedUser, RoomData);
	void deleteRoom(unsigned int roomId);
	unsigned int getRoomState(unsigned int roomId);
	void setRoomState(unsigned int roomId, unsigned int state);
	std::vector<RoomData> getRooms();
	std::vector<RoomData> getAvailableRooms();
	std::vector<std::string> getPlayersInRoom(unsigned int roomId);
	void addUserToRoom(unsigned int roomId, LoggedUser user);
	void removeUserFromRoom(unsigned int roomId, LoggedUser user);
	unsigned int getNewRoomId();
	bool roomExists(unsigned int roomId);
private:
	std::map<unsigned int, Room> m_rooms;
	static unsigned int _roomCounter;
	std::mutex _roomsMutex; //Used to block access to the rooms map when another user makes use of it.
};

