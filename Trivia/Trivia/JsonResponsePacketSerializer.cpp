#include "JsonResponsePacketSerializer.h"
#include <bitset>

/*
* Serialize error response of the buffer that will be sent to the client.
* Input: s - Error response struct.
* Output: Serialized error response.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(ErrorResponse s)
{
	json j = { { "message", s.message }};
	return createResponse(j, ERROR_CODE);
}

/*
* Serialize a login response to the buffer that will be sent to the client.
* Input: s - A Login response struct.
* Output: The serialized login response.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LoginResponse s)
{
	json j = { { "status", s.status } };
	return createResponse(j, SUCCESS_CODE);
}

/*
* Serializes the signup response that will be sent to the user.
* Input: s - A Signup response struct.
* Output: The serialized signup response.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SignupResponse s)
{
	json j = { { "status", s.status } };
	return createResponse(j, SUCCESS_CODE);
}

/*
This function serializes a logout response that will be sent to the user.
Input: s - A LogoutResponse struct.
Output: The serialized logout response.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LogoutResponse s)
{
	json j = { { "status", s.status } };
	return createResponse(j, SUCCESS_CODE);
}

/*
This function serializes a "get rooms" response that will be sent to the user.
Input: s - A GetRoomsResponse struct.
Output: The serialized "get rooms" response.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse s)
{
	json j = { { "Rooms", {}}, {"status", s.status} };
	for (auto iter : s.rooms)
	{
		j["Rooms"][iter.name] = { {"name", iter.name}, {"id", iter.id}, {"isActive", iter.isActive}, {"maxPlayers", iter.maxPlayers}, {"numOfQuestionInGame", iter.numOfQusetionInGame}, {"timePerQuestion", iter.timePerQuestion} };
	}
	return createResponse(j, SUCCESS_CODE);
}

/*
This function serializes a "get players in room" response that will be sent to the user.
Input: s - A GetPlayersInRoomResponse struct.
Output: The serialized "get players in room" response.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse s)
{
	json j = { { "PlayersInRoom", s.players }, {"status", s.status} };
	return createResponse(j, SUCCESS_CODE);
}

/*
This function serializes a "join room" response that will be sent to the user.
Input: s - A JoinRoomResponse struct.
Output: The serialized "join room" response.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse s)
{
	json j = { { "status", s.status } };
	return createResponse(j, SUCCESS_CODE);
}

/*
This function serializes a "create room" response that will be sent to the user.
Input: s - A CreateRoomResponse struct.
Output: The serialized "create room" response.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse s)
{
	json j = { {"roomId", s.roomId}, { "status", s.status} };
	return createResponse(j, SUCCESS_CODE);
}

/*
This function serializes a "get statistics" response that will be sent to the user.
Input: s - A GetStatisticsResponse struct.
Output: The serialized "get statistics" response.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetStatisticsResponse s)
{
	json j = { { "UserStatistics", json(s.userStatistics)}, {"HighScores", json(s.highScores)}, { "status", s.status } };
	return createResponse(j, SUCCESS_CODE);
}

/*
This function serializes a "close room" response that will be sent to the user.
Input: s - A CloseRoomResponse struct.
Output: The serialized "close room" response.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse s)
{
	json j = {{"status", s.status} };
	return createResponse(j, SUCCESS_CODE);
}

/*
This function serializes a "leave room" response that will be sent to the user.
Input: s - A LeaveRoomResponse struct.
Output: The serialized "leave room" response.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse s)
{
	json j = { {"status", s.status} };
	return createResponse(j, SUCCESS_CODE);
}

/*
This function serializes a "start game" response that will be sent to the user.
Input: s - A StartGameResponse struct.
Output: The serialized "start game" response.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(StartGameResponse s)
{
	json j = { {"status", s.status} };
	return createResponse(j, SUCCESS_CODE);
}

/*
This function serializes a "get room state" response that will be sent to the user.
Input: s - A GetRoomStateResponse struct.
Output: The serialized "get room state" response.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse s)
{
	json j = { {"hasGameBegun", s.hasGameBegun}, {"players", s.players}, {"answerCount", s.answerCount}, {"answerTimeout", s.answerTimeout}, {"status", s.status} };
	return createResponse(j, SUCCESS_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse s)
{
	json j = { { "players", {}}, {"status", s.status} };
	for (auto iter : s.results)
	{
		j["players"].push_back({ {"username", iter.username}, {"correctAnswersCount", iter.correctAnswerCount}, {"wrongAnswerCount", iter.wrongAnswerCount}, {"averageAnswerTime", iter.averageAnswerTime} ,{"pointsScored", iter.pointsScored} });
	}
	return createResponse(j, SUCCESS_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse s)
{
	json j = { {"status", s.status} };
	return createResponse(j, SUCCESS_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse s)
{
	json j = { {"status", s.status}, {"question", s.question}, {"answers", s.answers} };
	return createResponse(j, SUCCESS_CODE);	
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse s)
{
	json j = { {"status", s.status} };
	return createResponse(j, SUCCESS_CODE);
}

/*
* Converts a decimal number to a 4 bytes binary number. 
* Input: num - The decimal number
* Output: Vector of 4 bytes containig the number in binary.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::intTo4ByteBin(const int num)
{
	std::vector<unsigned char> binNum;
	std::string size = std::bitset<MESSAGE_BYTES_NUM * BYTE_SIZE>(num).to_string();
	for (int i = 0; i < size.size(); i += BYTE_SIZE)
	{
		binNum.push_back(std::bitset<BYTE_SIZE>(size.substr(i, BYTE_SIZE)).to_ulong());
	}
	return binNum;
}

/*
* Converts a decimal number to a single byte binary number.
* Input: num - The decimal number
* Output: Vector of a signle byte that contains the binary number.
*/
std::vector<unsigned char> JsonResponsePacketSerializer::intToByteBin(const int num)
{
	std::vector<unsigned char> binNum;
	std::string size = std::bitset<BYTE_SIZE>(num).to_string();
	for (int i = 0; i < size.size(); i += BYTE_SIZE)
	{
		binNum.push_back(std::bitset<BYTE_SIZE>(size.substr(i, BYTE_SIZE)).to_ulong());
	}
	return binNum;
}

/*
This function creates a serialized response from a json.
Input: j - The json that will be serialized.
	   code - Success/error code.
Output: The serialized response (json + headers).
*/
std::vector<unsigned char> JsonResponsePacketSerializer::createResponse(json j, int code)
{
	std::cout << j << std::endl;
	std::vector<unsigned char> buffer;
	auto binJ = json::to_bson(j);
	buffer.push_back(*(intToByteBin(code).begin()));
	auto size = intTo4ByteBin(binJ.size());
	buffer.insert(buffer.end(), size.begin(), size.end());
	buffer.insert(buffer.end(), binJ.begin(), binJ.end());
	return buffer;
}
