#include "StatisticsManager.h"
#include <unordered_map>

/*
A constructor function. This function creates a new "StatisticsManager" type object.
Input: IDatabase - A pointer to the server's database.
Output: None.
*/
StatisticsManager::StatisticsManager(IDatabase* database) :
    m_database(database)
{
}

/*
This function returns the top 5 highest obtained scores and the names of the users who obtained them.
Input: None.
Output: A map of the top 5 highest scores obtained with the names of the users who obtained them.
*/
std::map<std::string, int> StatisticsManager::getHighScore()
{
    return m_database->getHighScore();
}

/*
This function returns a variety of different statistics regarding the user.
Input: username - The username of the user whose statistics will be returned.
Output: A map that contains variety of different statistics regarding the user.
*/
std::map<std::string, int> StatisticsManager::getUserStatistics(std::string username)
{
    std::map<std::string, int> userStatistics;
    int numOfGamesPlayed = m_database->getNumOfPlayerGames(username);
    int avgResponseTime = 0;
    userStatistics.insert({"Number of games played", numOfGamesPlayed });
    userStatistics.insert({ "Number of correct answers", m_database->getNumOfCorrectAnswers(username) });
    userStatistics.insert({ "Number of incorrect answers", m_database->getNumOfTotalAnswers(username) - m_database->getNumOfCorrectAnswers(username) });
    if (numOfGamesPlayed > 0) //If no games have been played by the user, average response time can not be calculated.
        avgResponseTime = m_database->getPlayerAverageAnswerTime(username);
    else
        avgResponseTime = 0;
    userStatistics.insert({ "Average response time", avgResponseTime });
    return userStatistics;
}
