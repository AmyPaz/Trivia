#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"


class RequestHandlerFactory;
class MenuRequestHandler :
    public IRequestHandler
{
public:
    MenuRequestHandler(RoomManager& roomManager, StatisticsManager& statisticsManager, RequestHandlerFactory& requestHandlerFactory, LoggedUser user, IDatabase* database);
    ~MenuRequestHandler();
    virtual bool isRequestRelevant(RequestInfo& request) const override;
    virtual RequestResult handleRequest(RequestInfo& request) override;
    virtual void connectionLost() override;
private:
    LoggedUser m_user;
    RoomManager& m_roomManager;
    StatisticsManager& m_statisticsManager;
    RequestHandlerFactory& m_handlerFactory;
    IDatabase* m_database; //The database that is used by the program.

    RequestResult signout(RequestInfo request);
    RequestResult getAvailableRooms(RequestInfo request);
    RequestResult getPlayersInRoom(RequestInfo request);
    RequestResult getStatistics(RequestInfo request);
    RequestResult joinRoom(RequestInfo request);
    RequestResult createRoom(RequestInfo request);
    unsigned int getMaxQuestions();
};
