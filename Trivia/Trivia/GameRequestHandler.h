#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "Game.h"
#include <ctime>


class RequestHandlerFactory;
class GameRequestHandler :
    public IRequestHandler
{
public:
	GameRequestHandler(Game game, LoggedUser user, GameManager& gameManager, RequestHandlerFactory& handlerFactory);
	virtual bool isRequestRelevant(RequestInfo& request) const;
	virtual RequestResult handleRequest(RequestInfo& request);
	virtual void connectionLost();

private:
	Game m_game;
	LoggedUser m_user;
	GameManager& m_gameManager;
	RequestHandlerFactory& m_handlerFactory;
	time_t m_questionSentTime; //The time at which a question was sent to the client.
	RequestResult getQuestion(RequestInfo request);
	RequestResult submitAnswer(RequestInfo request);
	RequestResult getGameResults(RequestInfo request);
	RequestResult leaveGame(RequestInfo request);
};

