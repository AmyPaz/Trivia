#pragma once
#include <iostream>
#include <ctime>
#include <vector>

//The numbers of the different request codes.
enum requestCodes 
{
	LOGIN_REQUEST_CODE = 101, SIGNUP_REQUEST_CODE 
};
enum menuCodes
{
	SIGNOUT_REQUEST_CODE = 71, GET_ROOMS_REQUEST_CODE,
	GET_PLAYERS_REQUEST_CODE, GET_STATISTICS_REQUEST_CODE,
	JOIN_ROOM_REQUEST_CODE, CREATE_ROOM_REQUEST_CODE
};
enum roomAdminCodes
{
	START_GAME_REQUEST_CODE = 110, CLOSE_ROOM_REQUEST_CODE
};
enum roomMemberCodes
{
	LEAVE_ROOM_REQUEST_CODE = 121, GET_ROOM_STATE_REQUEST_CODE
};
enum gameCodes
{
	LEAVE_GAME_REQUEST_CODE = 131, GET_QUESTION_REQUEST_CODE, SUBMIT_ANSWER_REQUEST_CODE, GET_GAME_RESULT_REQUEST_CODE
};

struct RequestResult;
struct RequestInfo;

class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo& request) const = 0;
	virtual RequestResult handleRequest(RequestInfo& request) = 0;
	virtual void connectionLost() = 0;
};

typedef struct RequestInfo
{
	int requestId;
	time_t receivalTime; 
	std::vector<unsigned char> buffer; // the json request in bytes.
} RequestInfo;

typedef struct RequestResult
{
	std::vector<unsigned char> response; //The response from the server in bytes.
	IRequestHandler* newHandler;
} RequestResult;