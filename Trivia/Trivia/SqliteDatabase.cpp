#include "SqLiteDatabase.h"

/*
A constructor function. This function creates a new "SqliteDatabase" type object.
Input: None.
Output: None.
*/
SqliteDatabase::SqliteDatabase()
{
	_db = nullptr;
}

/*
A destructor function. Frees the memory that was allocated by an "SqliteDatabase" object.
Input: None.
Output: None.
*/
SqliteDatabase::~SqliteDatabase()
{
	closeDB();
}

/*
This function opens the database. If the database does not exist already, it creates a new one.
Input: None.
Ouput: None.
*/
void SqliteDatabase::openDB()
{
	if (_access(DB_FILE_NAME, 0) != FILE_EXISTS)
	{
		if (_access(DB_BACKUP_FILE_NAME, 0) == FILE_EXISTS)
		{
			//copy from backup
			std::ifstream  src(DB_BACKUP_FILE_NAME, std::ios::binary);
			std::ofstream  dst(DB_FILE_NAME, std::ios::binary);
			dst << src.rdbuf();
			src.close();
			dst.close();
			
			if (sqlite3_open(DB_FILE_NAME, &_db) != SQLITE_OK)
			{
				_db = nullptr;
				throw std::exception("Failed to open the Database.");
			}
		}
		else
		{
			// create new DB
			if (sqlite3_open(DB_FILE_NAME, &_db) != SQLITE_OK)
			{
				_db = nullptr;
				throw std::exception("Failed to open the Database.");
			}
			if (!createDB())
			{
				closeDB();
				throw std::exception("Failed to create the Database.");
			}
			throw std::exception("Database created without questions.");
		}
	}
	else
	{
		// open existing DB
		if (sqlite3_open(DB_FILE_NAME, &_db) != SQLITE_OK)
		{
			_db = nullptr;
			throw std::exception("Failed to open the Database.");
		}
	}
}

/*
This function checks if a user with a given username exists in the database.
Input: username - The username that will be searched in the database.
Ouput: boolean - whether or not the user exists.
*/
bool SqliteDatabase::doesUserExist(std::string username) 
{
	char* errMessage = nullptr;
	bool userExists = false;
	std::lock_guard<std::mutex> lock(_databaseMutex);
	int res = sqlite3_exec(_db, std::string("SELECT EXISTS(SELECT 1 FROM users WHERE username = \"" + username + "\") AS " + BOOL_RESULT + ";").c_str(), checkUserInfoCallback, (void*)&userExists, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception("Could not search for the wanted user in the database.");
	}
	return userExists;
}

/*
This function checks if the given password match the password of the given username.
Input: username - The username that will be searched in the database.
	   password - The password that will be validated.
Ouput: boolean - whether or not the password match the username.
*/
bool SqliteDatabase::doesPasswordMatch(std::string username, std::string password) 
{
	char* errMessage = nullptr;
	bool passwordMatches = false;
	std::lock_guard<std::mutex> lock(_databaseMutex);
	int res = sqlite3_exec(_db, std::string("SELECT EXISTS(SELECT 1 FROM users WHERE username = \"" + username + "\" AND password = \"" + password + "\") AS " + BOOL_RESULT + ";").c_str(), checkUserInfoCallback, (void*)&passwordMatches, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception("Could not search for the wanted user in the database.");
	}
	return passwordMatches;
}

/*
This function adds a new user to the database.
Input: username - The username of the new user.
	   password - The password of the new user.
	   emailAddress - The emailAddress of the new user.
Ouput: None.
*/
void SqliteDatabase::addNewUser(std::string username, std::string password, std::string emailAddress) 
{
	std::lock_guard<std::mutex> lock(_databaseMutex);
	if (!sendUpdateQueryToDB("INSERT INTO users(username, password, emailAddress) VALUES(\"" + username + "\", \"" + password + "\", \"" + emailAddress + "\");"))
	{
		throw std::exception(std::string("Failed to add the user \"" + username + "\".").c_str());
	}
}

/*
This function closes the database. 
Input: None.
Ouput: None.
*/
void SqliteDatabase::closeDB()
{
	sqlite3_close(_db);
	_db = nullptr;
}

/*
This function returns the average time it took the user to answer a question from the database.
Input: username - The username of the user whose average response time will be returned.
Output: The average response time of the user.
*/
float SqliteDatabase::getPlayerAverageAnswerTime(std::string username)
{
	char* errMessage = nullptr;
	float avgAnswerTime = 0;
	std::lock_guard<std::mutex> lock(_databaseMutex);
	int res = sqlite3_exec(_db, std::string("SELECT AVG(responseTime) AS " + std::string(AVERAGE) + " FROM (SELECT responseTime FROM statistics WHERE username = \"" + username + "\");").c_str(), averageCallback, (void*)&avgAnswerTime, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception("Could not retrive the average time it took the user to answer a question.");
	}
	return avgAnswerTime;

}

/*
This function returns the number of correct answers that the users has gotten so far from the database.
Input: username - The username of the user score will be returned.
Output: The amount of correct answers that the users has gotten so far.
*/
int SqliteDatabase::getNumOfCorrectAnswers(std::string username)
{
	char* errMessage = nullptr;
	int numOfCorrectAnswers = 0;
	std::lock_guard<std::mutex> lock(_databaseMutex);
	int res = sqlite3_exec(_db, std::string("SELECT COUNT(answeredCorrectly) AS " + std::string(COUNTER) + " FROM statistics WHERE username = \""+ username + "\" AND answeredCorrectly = " + std::to_string(true) + ";").c_str() , countCallback, (void*)&numOfCorrectAnswers, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception("Could not count the amount of questions to which the user has answered correctly.");
	}
	return numOfCorrectAnswers;
}

/*
This function the amount of answers that the user has given so far from the database.
Input: username - The username of the user whose sum of answers will be returned.
Output: The amount of answers that the user has given so far.
*/
int SqliteDatabase::getNumOfTotalAnswers(std::string username)
{
	char* errMessage = nullptr;
	int numOfAnswers = 0;
	std::lock_guard<std::mutex> lock(_databaseMutex);
	int res = sqlite3_exec(_db, std::string("SELECT COUNT(username) AS " + std::string(COUNTER) + " FROM statistics WHERE username = \"" + username +"\";").c_str(), countCallback, (void*)&numOfAnswers, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception("Could not count the amount of questions to which the user has answered.");
	}
	return numOfAnswers;
}

/*
This function returns the number of games that the user has participated in from the database.
Input: username - The username of the user whose amount of games will be returned.
Output: The number of games that the user has participated in so far.
*/
int SqliteDatabase::getNumOfPlayerGames(std::string username)
{
	char* errMessage = nullptr;
	int numOfGames = 0;
	std::lock_guard<std::mutex> lock(_databaseMutex);
	int res = sqlite3_exec(_db, std::string("SELECT COUNT(gameID) AS " + std::string(COUNTER) + " FROM (SELECT DISTINCT gameID FROM statistics WHERE username = \"" + username + "\");").c_str(), countCallback, (void*)&numOfGames, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception("Could not count the amount of games the user has participated in.");
	}
	return numOfGames;
}

/*
This function returns a map of the top five scores with the name of the users who obtained them.
Input: None.
Output: map of the top five scores with the name of the users who obtained them.
*/
std::map<std::string, int> SqliteDatabase::getHighScore()
{
	char* errMessage = nullptr;
	std::map<std::string, int> topFiveUsers;
	std::lock_guard<std::mutex> lock(_databaseMutex);
	int res = sqlite3_exec(_db, (std::string("SELECT DISTINCT username, SUM(points) AS ") + POINTS_SUM + " FROM statistics GROUP BY username, gameId ORDER BY " + POINTS_SUM + " DESC LIMIT 5;").c_str(), topUsersCallback, (void*)&topFiveUsers, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception("Could not retrive the highest scores.");
	}
	return topFiveUsers;
}

/*
This function returns questions from the database.
Input: num - The number of questions that will be returned.
Output: A vector that contains "num" random questions.
*/
std::vector<Question> SqliteDatabase::getQuestions(int num)
{
	char* errMessage = nullptr;
	std::vector<Question> questions;
	std::lock_guard<std::mutex> lock(_databaseMutex);
	int res = sqlite3_exec(_db, (std::string("SELECT* FROM questions ORDER BY RANDOM() LIMIT ") + std::to_string(num) + ";").c_str(), getQuestionsCallback, (void*)&questions, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception("Could not retrive questions.");
	}
	return questions;
}

/*
This function returns the total number of questions in the database.
Input: none
Output: the total number of questions.
*/
unsigned int SqliteDatabase::getMaxNumOfQuestions()
{
	char* errMessage = nullptr;
	unsigned int numMaxQuestions;
	std::lock_guard<std::mutex> lock(_databaseMutex);
	int res = sqlite3_exec(_db, (std::string("SELECT COUNT(*) FROM questions;")).c_str(), getMaxNumOfQuestionsCallback, (void*)&numMaxQuestions, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::exception("Could not retrive questions.");
	}
	return numMaxQuestions;
}

/*
This function sends a user's answer to the database.
Input: username - The user's username.
	   gameId - The ID of the game in which the user is participating.
	   responseTime - The time it took the user to answer the question.
	   answeredCorrectly - Whether or not the user answered correctly.
	   points - The amount of points that the user received from the question.
Output: None.
*/
void SqliteDatabase::submitAnswer(std::string username, unsigned int gameId, unsigned int responseTime, bool answeredCorrectly, int points)
{
	std::lock_guard<std::mutex> lock(_databaseMutex);
	if (!sendUpdateQueryToDB("INSERT INTO statistics(username, gameId, responseTime, answeredCorrectly, points) VALUES(\"" + username + "\", " + std::to_string(gameId) + ", " + std::to_string(responseTime) + ", " + std::to_string(answeredCorrectly) + ", " + std::to_string(points) + ");"))
	{
		throw std::exception(std::string("Failed to submit the answer.").c_str());
	}
}

/*
This function creates a new database.
Input: None.
Output: None.
*/
bool SqliteDatabase::createDB()
{
	//Creates the users table.
	return sendUpdateQueryToDB("CREATE TABLE users(username TEXT PRIMARY KEY NOT NULL, password TEXT NOT NULL, emailAddress TEXT NOT NULL);")
		//Creates the statistics table.
		&& sendUpdateQueryToDB("CREATE TABLE statistics(username TEXT NOT NULL, gameId INT NOT NULL, responseTime INT NOT NULL, answeredCorrectly BOOL NOT NULL, points INT NOT NULL, FOREIGN KEY(username) REFERENCES users(username));")
		&& sendUpdateQueryToDB("CREATE TABLE questions(ID	INTEGER NOT NULL UNIQUE, question TEXT NOT NULL UNIQUE, ans1 TEXT NOT NULL, ans2 TEXT NOT NULL, ans3 TEXT, ans4 TEXT, PRIMARY KEY(ID AUTOINCREMENT)	);");
}

/*
This function sends update queries to the database.
Input: sqlStatement - The query that will be sent to the database.
Output: boolean - whether or not the sending proccess succeeded.
*/
bool SqliteDatabase::sendUpdateQueryToDB(std::string sqlStatement)
{
	char* errMessage = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	return (res == SQLITE_OK);
}

/*
A callback function. This function is used to check something regarding a user.
Input: data - The parameter that is passed to the function (the boolean result).
argc - The amount of columns in the table that is returned (1).
argv - The answer to the query.
azColName - The names of the columns in the table.
Output: 0.
*/
int checkUserInfoCallback(void* data, int argc, char** argv, char** azColName)
{
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == BOOL_RESULT)
		{
			*((bool*)data) = ((bool)atoi(argv[i]));
		}
	}
	return 0;
}

/*
A callback function. This function is used to count intances of things in the database.
Input: data - The parameter that is passed to the function (the int result).
argc - The amount of columns in the table that is returned (1).
argv - The answer to the query.
azColName - The names of the columns in the table.
Output: 0.
*/
int countCallback(void* data, int argc, char** argv, char** azColName)
{
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == COUNTER)
		{
			*((int*)data) = atoi(argv[i]);
		}
	}
	return 0;
}

/*
A callback function. This function is used to average of numbers in the database.
Input: data - The parameter that is passed to the function (the float result).
argc - The amount of columns in the table that is returned (1).
argv - The answer to the query.
azColName - The names of the columns in the table.
Output: 0.
*/
int averageCallback(void* data, int argc, char** argv, char** azColName)
{
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == AVERAGE)
		{
			*((float*)data) = std::stof(argv[i]);
		}
	}
	return 0;
}

/*
A callback function. This function is used to get the top scores and the users who scored them from the database.
Input: data - The parameter that is passed to the function (the map that will contain the scores and names).
argc - The amount of columns in the table that is returned (2).
argv - The answer to the query.
azColName - The names of the columns in the table.
Output: 0.
*/
int topUsersCallback(void* data, int argc, char** argv, char** azColName)
{
	std::string username;
	int score;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == USERNAME)
		{
			username = argv[i];
		}
		else if (std::string(azColName[i]) == POINTS_SUM)
		{
			score = atoi(argv[i]);
		}
	}
	((std::map<std::string, int>*)data)->insert(std::pair<std::string, int>(username, score));
	return 0;
}

/*
A callback function. This function is used to get questions from the database.
Input: data - The parameter that is passed to the function (the vector that will contain questions).
argc - The amount of columns in the table that is returned.
argv - The answer to the query.
azColName - The names of the columns in the table.
Output: 0.
*/
int getQuestionsCallback(void* data, int argc, char** argv, char** azColName)
{
	std::string question;
	std::vector<std::string> answers;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == QUESTION)
		{
			question = argv[i];
		}
		else if (std::string(azColName[i]).find(ANS) != std::string::npos)
		{
			if(argv[i] != nullptr)
				answers.push_back(argv[i]);
		}
	}
	Question fullQuestion = Question(question, answers);
	((std::vector<Question>*)data)->push_back(fullQuestion);
	return 0;
}

/*
A callback function. This function is used to get the total number of questions in the database.
Input: data - The parameter that is passed to the function (the vector that will contain questions).
argc - The amount of columns in the table that is returned.
argv - The answer to the query.
azColName - The names of the columns in the table.
Output: 0.
*/
int getMaxNumOfQuestionsCallback(void* data, int argc, char** argv, char** azColName)
{
	*(unsigned int*)data = std::atoi(argv[0]);
	return 0;
}
