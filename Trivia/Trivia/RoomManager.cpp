#include "RoomManager.h"

unsigned int RoomManager::_roomCounter = 0;

/*
* creats a new game room
* Input: first user to start in room, room data struct
* Output: none
*/
void RoomManager::createRoom(LoggedUser user, RoomData data)
{
	if (data.maxPlayers < MIN_NUM_PLAYERS) throw std::exception(std::string("Minimum number of players is " + std::to_string(MIN_NUM_PLAYERS) + ".").c_str());
	else if (data.numOfQusetionInGame < MIN_NUM_QUSTIONS) throw std::exception(std::string("Minimum number of questions is " + std::to_string(MIN_NUM_QUSTIONS) + ".").c_str());
	else if(data.timePerQuestion < MIN_TIME_QUSTIONS) throw std::exception(std::string("Minimum time per question is " + std::to_string(MIN_TIME_QUSTIONS) + ".").c_str());

	std::lock_guard<std::mutex> lock(_roomsMutex);
	for (auto room : m_rooms)
	{
		if (room.second.getRoomData().name == data.name)
		{
			throw std::exception("The chosen room name is already taken.");
		}
	}
	Room newRoom = Room(data);
	newRoom.addUser(user);
	m_rooms.insert({ data.id, newRoom});
}

/*
* delets a game room
* Input: room id
* Output: none
*/
void RoomManager::deleteRoom(unsigned int roomId)
{
	std::lock_guard<std::mutex> lock(_roomsMutex);
	m_rooms.erase(roomId);
}

/*
* get game room state
* Input: room id
* Output: room state
*/
unsigned int RoomManager::getRoomState(unsigned int roomId)
{
	std::lock_guard<std::mutex> lock(_roomsMutex);
	return m_rooms.at(roomId).getRoomData().isActive;
}

/*
This function set the state of a room.
Input: roomId - The id of the room whose state will be changed.
	   state - The new state of the room.
Output: None.
*/
void RoomManager::setRoomState(unsigned int roomId, unsigned int state)
{
	std::lock_guard<std::mutex> lock(_roomsMutex);
	m_rooms.at(roomId).setIsActive(state);
}

/*
* get all game rooms
* Input: none
* Output: vector of data of all rooms.
*/
std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> roomsData;
	std::lock_guard<std::mutex> lock(_roomsMutex);
	for (std::pair<unsigned int, Room> room : m_rooms)
	{
		roomsData.push_back(room.second.getRoomData());
	}
	return roomsData;
}

/*
* get all the available game rooms to join
* Input: none
* Output: vector of data of all rooms.
*/
std::vector<RoomData> RoomManager::getAvailableRooms()
{
	std::vector<RoomData> roomsData;
	std::lock_guard<std::mutex> lock(_roomsMutex);
	for (std::pair<unsigned int, Room> room : m_rooms)
	{
		//dont send room if game started or room is full
		if (room.second.getRoomData().isActive == false && room.second.getRoomData().maxPlayers != room.second.getNumberOfUsers())
			roomsData.push_back(room.second.getRoomData());
	}
	return roomsData;
}

/*
* This function returns all the players in a room.
* Input: roomId - The id of the room.
* Output: All the players in the room.
*/
std::vector<std::string> RoomManager::getPlayersInRoom(unsigned int roomId)
{
	std::lock_guard<std::mutex> lock(_roomsMutex);
	if (m_rooms.find(roomId) == m_rooms.end()) throw std::exception("Could not retrieve the names of the users in the room.");
	return m_rooms.at(roomId).getAllUsers();
}

/*
* This function adds a user to a room.
* Input: roomId - The id of the room to which the user will be added.
* Output: None.
*/
void RoomManager::addUserToRoom(unsigned int roomId, LoggedUser user)
{
	std::lock_guard<std::mutex> lock(_roomsMutex);
	if (m_rooms.find(roomId) == m_rooms.end()) {
		throw std::exception("The wanted room can not be joined.");
	}
	else if (m_rooms.at(roomId).getRoomData().maxPlayers <= m_rooms.at(roomId).getAllUsers().size())
	{
		throw std::exception("The wanted room is at max capacity.");
	}
	else if (m_rooms.at(roomId).getRoomData().isActive == true)
	{
		throw std::exception("The wanted room has already started the game.");
	}

	m_rooms.at(roomId).addUser(user);
}

/*
* This function removes a user to a room.
* Input: roomId - The id of the room to which the user will be removed.
* Output: None.
*/
void RoomManager::removeUserFromRoom(unsigned int roomId, LoggedUser user)
{
	if (roomExists(roomId))
	{
		std::lock_guard<std::mutex> lock(_roomsMutex);
		m_rooms.at(roomId).removeUser(user);
		if (m_rooms.at(roomId).getNumberOfUsers() == 0)
		{
			m_rooms.erase(roomId);
		}
	}
}

/*
This function returns a new room id that can be used.
Input: None.
Output: A roomId that can be used to create a new room.
*/
unsigned int RoomManager::getNewRoomId()
{
	_roomCounter++;
	return _roomCounter;
}

/*
This function checks if a room exists;
Input: roomId - The room to be searched.
Output: Whether or not the wanted room exists.
*/
bool RoomManager::roomExists(unsigned int roomId)
{
	std::lock_guard<std::mutex> lock(_roomsMutex);
	return m_rooms.find(roomId) != m_rooms.end();
}

