#pragma comment (lib, "ws2_32.lib")
#include "Server.h"
#include "WSAInitializer.h"
#include <iostream>
#include <exception>
#include <thread>

int main()
{
	bool exitFlag = false;
	std::string userInput;
	WSAInitializer wsaInit;
	Server s;

	std::thread t1(&Server::run, std::ref(s));
	t1.detach();

	while (!exitFlag)
	{
		std::cin >> userInput;
		if (userInput == "EXIT") exitFlag = true;
	}
	return 0;
}