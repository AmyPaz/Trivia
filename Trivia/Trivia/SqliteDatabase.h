#pragma once
#include "IDatabase.h"
#include "sqlite3.h"
#include <string>
#include <utility>
#include <mutex>
#include <fstream>
#include "Question.h"

//The number that the function "_access" returns if a file already exists.
#define FILE_EXISTS 0 

//The column that saves the boolean answer to the query that was sent to the SQL database regarding the users' info.
#define BOOL_RESULT "boolResult" 

//The column that saves the answer when using queries that count instances of things.
#define COUNTER "counter" 

//The column that saves the answer when using queries that count instances of things.
#define AVERAGE "average" 

//The column that sums the number of points each user had in a specific game.
#define POINTS_SUM "pointsSum"

//The column that SAVES the name of the user in the database.
#define USERNAME "username"

//The column that SAVES the qustion in the database.
#define QUESTION "question"

//The column that SAVES the answers in the database.
#define ANS "ans"

//The name of the sqlite file of the database.
#define DB_FILE_NAME "TriviaDB.sqlite" 

//The name of the backup sqlite file of the database.
#define DB_BACKUP_FILE_NAME "../TriviaBackupDB.sqlite" 

class SqliteDatabase : public IDatabase
{
public:
	SqliteDatabase();
	virtual ~SqliteDatabase() override;
	virtual void openDB() override;
	virtual bool doesUserExist(std::string username) override;
	virtual bool doesPasswordMatch(std::string username, std::string password) override;
	virtual void addNewUser(std::string username, std::string password, std::string emailAddress) override;
	virtual void closeDB() override;
	virtual float getPlayerAverageAnswerTime(std::string username) override;
	virtual int getNumOfCorrectAnswers(std::string username) override;
	virtual int getNumOfTotalAnswers(std::string username) override;
	virtual int getNumOfPlayerGames(std::string username) override;
	virtual std::map<std::string, int> getHighScore() override;
	virtual std::vector<Question> getQuestions(int num) override;
	virtual unsigned int getMaxNumOfQuestions() override;
	virtual void submitAnswer(std::string username, unsigned int gameId, unsigned int responseTime, bool answeredCorrectly, int points);

private:
	sqlite3* _db; //An handle to the database.
	std::mutex _databaseMutex; //Used to block users from accessing the database when it is already in use.
	bool createDB();
	bool sendUpdateQueryToDB(std::string sqlStatement);
	
};

//Callback functions.
int checkUserInfoCallback(void* data, int argc, char** argv, char** azColName);
int countCallback(void* data, int argc, char** argv, char** azColName);
int averageCallback(void* data, int argc, char** argv, char** azColName);
int topUsersCallback(void* data, int argc, char** argv, char** azColName);
int getQuestionsCallback(void* data, int argc, char** argv, char** azColName);
int getMaxNumOfQuestionsCallback(void* data, int argc, char** argv, char** azColName);