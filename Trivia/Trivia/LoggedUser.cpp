#include "LoggedUser.h"

/*
A constructor function. This function creates a new "LoggedUser" type object.
Input: username - The name of the logged user.
Output: None.
*/
LoggedUser::LoggedUser(const std::string& username)
{
    m_username = username;
}

/*
A getter function. This function returns the name of the logged user.
Input: None.
Output: The username of the logged user.
*/
std::string LoggedUser::getUsername() const
{
    return m_username;
}

/*
== operator. This function is used to compare two LoggedUsers.
Input: other - The other LoggedUser that will be compared to "this".
Output: boolean - Whether or not the logged users are the same.
*/
bool LoggedUser::operator==(const LoggedUser& other) const
{
    return this->m_username == other.m_username;
}

bool LoggedUser::operator<(const LoggedUser& lg) const
{
    return m_username < lg.m_username;
}
