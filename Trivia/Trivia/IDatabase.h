#pragma once
#include <iostream>
#include <io.h>
#include <vector>
#include <map>
#include "LoggedUser.h"
#include "Question.h"

//An interface class for the database class to use.
class IDatabase
{
public:
	virtual ~IDatabase() = default;
	virtual void openDB() = 0;
	virtual bool doesUserExist(std::string username) = 0;
	virtual bool doesPasswordMatch(std::string username, std::string password) = 0;
	virtual void addNewUser(std::string username, std::string password, std::string emailAddress) = 0;
	virtual void closeDB() = 0;
	virtual float getPlayerAverageAnswerTime(std::string username) = 0;
	virtual int getNumOfCorrectAnswers(std::string username) = 0;
	virtual int getNumOfTotalAnswers(std::string username) = 0;
	virtual int getNumOfPlayerGames(std::string username) = 0;
	virtual std::map<std::string, int> getHighScore() = 0;
	virtual std::vector<Question> getQuestions(int num) = 0;
	virtual unsigned int getMaxNumOfQuestions() = 0;
	virtual void submitAnswer(std::string username, unsigned int gameId, unsigned int responseTime, bool answeredCorrectly, int points) = 0;
};

