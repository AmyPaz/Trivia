#pragma once
#include "IRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;
class RoomMemberRequestHandler :
    public IRequestHandler
{
public:
    RoomMemberRequestHandler(RoomManager& roomManager, RequestHandlerFactory& requestHandlerFactory, LoggedUser user, Room room);
    ~RoomMemberRequestHandler() = default;
    virtual bool isRequestRelevant(RequestInfo & request) const override;
    virtual RequestResult handleRequest(RequestInfo & request) override;
    virtual void connectionLost() override;
private:
    Room m_room;
    LoggedUser m_user;
    RoomManager& m_roomManager;
    RequestHandlerFactory& m_handlerFactory;

    RequestResult leaveRoom(RequestInfo requestInfo);
    RequestResult getRoomState(RequestInfo requestInfo);
};

