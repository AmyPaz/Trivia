#pragma once
#include "IRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"
#include "JsonRequestPacketDeserializer.h"
#include "LoginManager.h"

class RequestHandlerFactory;
class LoginRequestHandler :
    public IRequestHandler
{
public:
    LoginRequestHandler(RequestHandlerFactory& handlerFactory);
    ~LoginRequestHandler();
    virtual bool isRequestRelevant(RequestInfo& request) const override ;
    virtual RequestResult handleRequest(RequestInfo& request) override;
    virtual void connectionLost();
private:
    RequestHandlerFactory& m_handlerFactory;
    LoginManager& m_loginManager;
    RequestResult login(RequestInfo request);
    RequestResult signup(RequestInfo request);
};

