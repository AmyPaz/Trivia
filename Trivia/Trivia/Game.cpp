#include "Game.h"

/*
A constructor function. This function craetes a new "Game" type object.
Input: users - The users who participate in the game.
       Question - The questions that the user will answer during the game.
Output: None.
*/
Game::Game(std::vector<LoggedUser> users, std::vector<Question> questions, unsigned int id, unsigned int answerTimeout) :
    m_questions(questions), m_id(id), m_answerTimeout(answerTimeout)
{
    for (unsigned int i = 0; i < users.size(); i++)
    {
        GameData gameData = {questions[0], 0 , 0 , 0 };
        m_players.insert({ users[i], gameData });
    }
}

/*
This function returns the current question that the user has to answer.
Input: user - The user that will be receiving the question.
Output: The next question that the user has to answer.
*/
Question Game::getQuestionForUser(LoggedUser user)
{
    m_players.at(user).currentQuestion = m_questions[m_players.at(user).correctAnswerCount + m_players.at(user).wrongAnswerCount];
    return m_players.at(user).currentQuestion;
}

/*
This function submits an answer to the question.
Input: user - The user who submits the answer.
       answer - The answer that the user submits.
       timeToAnswer - The time it took the player to answer the question.
Output: Whehter or not the answer was correct.
*/
bool Game::submitAnswer(LoggedUser user, unsigned int answer, unsigned int timeToAnswer)
{
    unsigned int questionsAnswerd = m_players.at(user).correctAnswerCount + m_players.at(user).wrongAnswerCount;
    m_players.at(user).averageAnswerTime = (timeToAnswer + m_players.at(user).averageAnswerTime * questionsAnswerd) / (float)(questionsAnswerd + 1);
    if (timeToAnswer < m_answerTimeout && m_players.at(user).currentQuestion.getCorrentAnswer() == m_players.at(user).currentQuestion.getPossibleAnswers()[answer])
    {
        m_players.at(user).correctAnswerCount++;
        return true;
    }
    m_players.at(user).wrongAnswerCount++;
    return false;
}

/*
This function adds points to the user's sum of points.
Input: user - The user who receives the points.
       points - The amount of points that the user received.
Output: None.
*/
void Game::addPoints(LoggedUser user, int points)
{
    m_players.at(user).pointsScored += points;
}

/*
This function adds the user to the game.
Input: The user.
Output: None.
*/
void Game::addPlayer(LoggedUser user)
{
    GameData gameData = { m_questions[0], 0 , 0 , 0 };
    m_players.insert({ user, gameData });
}

/*
This function is used if a players leaves the game.
Input: The user.
Output: None.
*/
void Game::playerLeft(LoggedUser user)
{
    if (m_players.find(user) == m_players.end())
    {
        throw std::exception("You are not participating in this game.");
    }
    m_players.find(user)->second.wrongAnswerCount = m_questions.size() - m_players.find(user)->second.correctAnswerCount;
}

/*
"==" operator. Used to compare two games.
Input: game - The game that "this" game will be compared to.
Output: Whether or not the two games are the same.
*/
bool Game::operator==(const Game& game) const
{
    return m_id == game.m_id;
}

/*
A getter function. This function returns the "m_id" field of the game.
Input: None.
Output: The "m_id" field of the game.
*/
int Game::getGameId() const
{
    return m_id;
}

/*
A getter function. This function returns the "m_answerTimeout" field of the game.
Input: None.
Output: The "m_answerTimeout" field of the game.
*/
unsigned int Game::getAnswerTimeout() const
{
    return m_answerTimeout;
}

/*
This function returns the results of the game.
Input: None.
Output: A vector that contains the results of the game.
*/
std::vector<PlayerResult> Game::getResults() const
{
    std::set<PlayerResult> results;
    PlayerResult result;
    for (auto const player : m_players)
    {
        if (player.second.correctAnswerCount + player.second.wrongAnswerCount < m_questions.size())
            return std::vector<PlayerResult>();
        result.username = player.first.getUsername();
        result.averageAnswerTime = player.second.averageAnswerTime;
        result.correctAnswerCount = player.second.correctAnswerCount;
        result.pointsScored = player.second.pointsScored;
        result.wrongAnswerCount = player.second.wrongAnswerCount;
        results.insert(result);
    }
    return std::vector<PlayerResult>(results.begin(), results.end());
}


