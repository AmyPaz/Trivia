#include "Server.h"
#include <iostream>

/*
A constructor function. This function creates a new "Server" type object.
Input: None.
Output: None.
*/
Server::Server() : 
	m_database(new SqliteDatabase()), m_handlerFactory(m_database), m_comunicator(m_handlerFactory)
{
}

/*
A destructor function. This function frees the memory that was allocated by the constructor.
Input: None.
Output: None.
*/
Server::~Server()
{
	delete m_database;
}

/*
This function runs the server.
Input: None.
Output: None.
*/
void Server::run()
{
	try
	{
		m_comunicator.startHandleRequests();
	}
	catch (std::exception e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
}
