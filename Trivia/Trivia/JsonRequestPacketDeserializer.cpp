#include "JsonRequestPacketDeserializer.h"
#include <bitset>

/*
This function deserializes a login request.
Input: buffer - The buffer that was received from the user, contians olny the login request.
Output: The login request that contains the data from the buffer.
*/
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(const std::vector<unsigned char> buffer)
{ 
	auto jsonStr = json::parse(std::string(buffer.begin(), buffer.end()));
	auto loginRequest = jsonStr.get<LoginRequest>();
    return loginRequest;
}

/*
This function deserializes a signup request.
Input: buffer - The buffer that was received from the user, contians only the signup request.
Output: The signup request that contains the data from the buffer.
*/
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(std::vector<unsigned char> buffer)
{
    auto jsonStr = json::parse(std::string(buffer.begin(), buffer.end()));
    auto signupRequest = jsonStr.get<SignupRequest>();
    return signupRequest;
}

/*
This function deserializes a GetPlayersInRoomRequest.
Input: buffer - The buffer that was received from the user, contiaing only the data.
Output: The GetPlayersInRoomRequest that contains the data from the buffer.
*/
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(const std::vector<unsigned char> buffer)
{
    auto jsonStr = json::parse(std::string(buffer.begin(), buffer.end()));
    auto getPlayersRequest = jsonStr.get<GetPlayersInRoomRequest>();
    return getPlayersRequest;
}

/*
This function deserializes a JoinRoomRequest.
Input: buffer - The buffer that was received from the user, contiaing only the data.
Output: The JoinRoomRequest that contains the data from the buffer.
*/
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(const std::vector<unsigned char> buffer)
{
    auto jsonStr = json::parse(std::string(buffer.begin(), buffer.end()));
    auto joinRoomRequest = jsonStr.get<JoinRoomRequest>();
    return joinRoomRequest;
}

/*
This function deserializes a CreateRoomRequest.
Input: buffer - The buffer that was received from the user, contiaing only the data.
Output: The CreateRoomRequest that contains the data from the buffer.
*/
CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(const std::vector<unsigned char> buffer)
{
    auto jsonStr = json::parse(std::string(buffer.begin(), buffer.end()));
    auto createRoomRequest = jsonStr.get<CreateRoomRequest>();
    return createRoomRequest;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(const std::vector<unsigned char> buffer)
{
    auto jsonStr = json::parse(std::string(buffer.begin(), buffer.end()));
    auto submitAnswerRequest = jsonStr.get<SubmitAnswerRequest>();
    return submitAnswerRequest;
}

/*
* Converts a binary 4 byte number to a long int.
* Input: buffer - Buffer of the number
* Output: The decimal number (long).
*/
long JsonRequestPacketDeserializer::bin4byteToInt(const std::vector<unsigned char> buffer)
{
    std::string temp = "";
    for (int i = 0; i < MESSAGE_BYTES_NUM; i++)
    {
        int tempnum = buffer[i];
        temp.append(std::bitset<BYTE_SIZE>(tempnum).to_string());
    }
    return std::bitset<BYTE_SIZE * MESSAGE_BYTES_NUM>(temp).to_ulong();
}

/*
* converts a single byte number to a long int.
* Input: buffer - Buffer of the number.
* Output: The decimal number (long).
*/
long JsonRequestPacketDeserializer::bin1byteToInt(const std::vector<unsigned char> buffer)
{
    return *buffer.begin();
}

/*
This function is used to convert a json to a LoginRequest.
Input: j - The json.
       loginRequest - The LoginRequest.
Output: None.
*/
void from_json(const json& j, LoginRequest& loginRequest) {
    j.at("username").get_to(loginRequest.username);
    j.at("password").get_to(loginRequest.password);
}

/*
This function is used to convert a json to a SignupRequest.
Input: j - The json.
       loginRequest - The SignupRequest.
Output: None.
*/
void from_json(const json& j, SignupRequest& signupRequest) {
    j.at("username").get_to(signupRequest.username);
    j.at("password").get_to(signupRequest.password);
    j.at("email").get_to(signupRequest.email);
}

/*
This function is used to convert a json to a GetPlayersInRoomRequest.
Input: j - The json.
       getPlayersInRoomRequest - The GetPlayersInRoomRequest.
Output: None.
*/
void from_json(const json& j, GetPlayersInRoomRequest& getPlayersInRoomRequest)
{
    j.at("roomId").get_to(getPlayersInRoomRequest.roomId);
}

/*
This function is used to convert a json to a JoinRoomRequest.
Input: j - The json.
       joinRoomRequest - The JoinRoomRequest.
Output: None.
*/
void from_json(const json& j, JoinRoomRequest& joinRoomRequest)
{
    j.at("roomId").get_to(joinRoomRequest.roomId);
}

/*
This function is used to convert a json to a CreateRoomRequest.
Input: j - The json.
       createRoomRequest - The CreateRoomRequest.
Output: None.
*/
void from_json(const json& j, CreateRoomRequest& createRoomRequest)
{
    j.at("roomName").get_to(createRoomRequest.roomName);
    j.at("maxUsers").get_to(createRoomRequest. maxUsers);
    j.at("questionCount").get_to(createRoomRequest. questionCount);
    j.at("answerTimeout").get_to(createRoomRequest. answerTimeout);
}

void from_json(const json& j, SubmitAnswerRequest& submitAnswerRequest)
{
    j.at("answer").get_to(submitAnswerRequest.answer);
}
