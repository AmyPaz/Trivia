﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for Game.xaml
    /// </summary>
    public partial class Game : Window
    {
        /*
        A constructor function. This function creates a new "Game" type object.
        Input: timePerQuestion - The time that the participants of the game will have to answer each question.
               numberOfQuestions - The number of questions in the game.
        Output: None.
        */
        public Game(uint timePerQuestion, uint numberOfQuestions)
        {
            InitializeComponent();
            DataContext = new questionView(timePerQuestion, numberOfQuestions);

        }

        /*
        This function handles the closing event of the window. 
        */
        private void Window_Closed(object sender, EventArgs e)
        {
            DataContext = null;
            Communicator.send(JsonRequestPacketSeserializer.serializeLeaveGameRequest());
            Communicator.unserializedResponse response = Communicator.recive();
            if (response.code == (int)ResponseCodes.ERROR_CODE)
            {
                var answer = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.ErrorResponse>(response.buffer.ToArray());
                MessageBox.Show(answer.message);
            }
            this.Owner.Show();
        }
    }
}
