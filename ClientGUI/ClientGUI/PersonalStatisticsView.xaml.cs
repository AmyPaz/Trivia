﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for PersonalStatisticsView.xaml
    /// </summary>
    public partial class PersonalStatisticsView : UserControl
    {
        public PersonalStatisticsView()
        {
            InitializeComponent();
            getStatistics();
        }

        /*
         * gets the information from the server
         */
        private void getStatistics()
        {
            Communicator.send(JsonRequestPacketSeserializer.serializeGetStatisticsRequest());
            var answer = Communicator.recive();
            if (answer.code == (int)ResponseCodes.ERROR_CODE)
            {
                numGamesPlayedHeader.Visibility = Visibility.Hidden;
                numCorrectAnsHeader.Visibility = Visibility.Hidden;
                numIncorrectAnsHeader.Visibility = Visibility.Hidden;
                avgresponseTimeHeader.Visibility = Visibility.Hidden;
                errorText.Text = "Could not retrive your personal statistics.";
                errorText.Visibility = Visibility.Visible;

            }
                
            else if (answer.code == (int)ResponseCodes.SUCCESS_CODE)
            {
                var response = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.GetStatisticsResponse>(answer.buffer.ToArray());
                numGamesPlayed.Text = Convert.ToString(response.UserStatistics["Number of games played"]);
                numCorrectAns.Text = Convert.ToString(response.UserStatistics["Number of correct answers"]);
                numIncorrectAns.Text = Convert.ToString(response.UserStatistics["Number of incorrect answers"]);
                avgResponseTime.Text = Convert.ToString(response.UserStatistics["Average response time"]);
            }
        }
    }
}
