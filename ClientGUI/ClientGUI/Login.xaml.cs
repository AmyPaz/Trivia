﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        /*
        A constructor function. This function creates and shows the login window.
        Input: None.
        Output: None.
        */
        public Login()
        {
            InitializeComponent();
        }

        /*
        This function handles the event "click" of the hyperlink that opens the signup screen.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Output: None.
        */
        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Signup signupScreen = new Signup();
            signupScreen.Show();
            signupScreen.Owner = this.Owner;
            this.Close();
            signupScreen.Owner.Hide();
        }

        /*
        This function handles the event when enter is pressed in the form.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Ouput: None.
        */
        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                LoginButton_Click(null, null);
            }
        }

        /*
        This function handles the event "click" of the "Log in" button.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Ouput: None.
        */
        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            if (username.Text == "" || password.Password == "")
            {
                errorText.Text = "Please make sure to fill all of the fields above."; 
                errorText.Visibility = Visibility.Visible;
            }
            else
            {
                Communicator.send(JsonRequestPacketSeserializer.serializeLoginRequest(username.Text, password.Password));
                Communicator.unserializedResponse response = Communicator.recive();
                if (response.code == (int)ResponseCodes.ERROR_CODE)
                {
                    var answer = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.ErrorResponse>(response.buffer.ToArray());
                    errorText.Text = answer.message;
                    errorText.Visibility = Visibility.Visible;
                }
                else
                {
                    Application.Current.Properties["isLoggedin"] = true;
                    Application.Current.MainWindow.Resources["isLoggedin"] = true;
                    ((MainWindow)Application.Current.MainWindow).loginButton.IsEnabled = false;
                    ((MainWindow)Application.Current.MainWindow).signupButton.IsEnabled = false;
                    this.Close();
                }
            }
        }

        /*
        This function handles the event "click" of the "Back" button.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Ouput: None.
        */
        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /*
         * handle when window closes
         * shows the main window
         */
        private void closed_Handler(object sender, EventArgs e)
        {
            this.Owner.Show();
        }
    }
}
