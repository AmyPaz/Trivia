﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Windows;

namespace ClientGUI
{
    public static class Communicator
    {
        private static int SERVER_PORT = 8853;
        private static string SERVER_IP = "127.0.0.1";
        private static NetworkStream clientStream = null;
        private static TcpClient client = new();
        public struct unserializedResponse
        {
            public int code;
            public int size;
            public List<byte> buffer;

            public unserializedResponse(int code, byte[] buffer)
            {
                this.code = code;
                this.size = buffer.Length;
                this.buffer = new List<byte>();
                this.buffer.AddRange(buffer);
            }
        }

        public static void connect()
        {
            IPEndPoint serverEndPoint = new(IPAddress.Parse(SERVER_IP), SERVER_PORT);
            if (clientStream == null)
            {
                try
                {
                    client.Connect(serverEndPoint);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Server error", MessageBoxButton.OK, MessageBoxImage.Error);
                    MessageBoxResult result = MessageBox.Show("Retry to connect to server?", "", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if(result == MessageBoxResult.Yes) connect();
                    else Environment.Exit(1);
                }
                clientStream = client.GetStream();
            }
        }

        public static void closeConnection()
        {
            clientStream.Close();
            clientStream = null;
            client.Close();
        }

        public static void send(byte[] buffer)
        {
            try
            {
                clientStream.Write(buffer);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Server error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1);
            }
        }

        public static unserializedResponse recive()
        {
            unserializedResponse response = new();
            try
            {
                byte[] temp = new byte[4];
                clientStream.Read(temp, 0, 1);
                response.code = Convert.ToInt32(temp[0]);
                clientStream.Read(temp, 0, 4);
                Array.Reverse(temp);
                response.size = BitConverter.ToInt32(temp, 0);
                byte[] buffer = new byte[response.size];
                clientStream.Read(buffer, 0, response.size);
                response.buffer = new List<byte>();
                response.buffer.AddRange(buffer);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Server error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1);
            }
            return response;
        }
    }
}
