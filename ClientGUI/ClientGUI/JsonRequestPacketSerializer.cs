﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.IO;

namespace ClientGUI
{
    //All the request codes that are used by the client.
    public enum requestCode
    {
        //Menu related request codes.
        SIGNOUT_REQUEST_CODE = 71, GET_ROOMS_REQUEST_CODE,
        GET_PLAYERS_REQUEST_CODE, GET_STATISTICS_REQUEST_CODE,
        JOIN_ROOM_REQUEST_CODE, CREATE_ROOM_REQUEST_CODE,

        //Login related request codes.
        LOGIN_REQUEST_CODE = 101, SIGNUP_REQUEST_CODE,

        //Room related request codes.
        START_GAME_REQUEST_CODE = 110,
        CLOSE_ROOM_REQUEST_CODE,
        
        LEAVE_ROOM_REQUEST_CODE = 121,
        GET_ROOM_STATE_REQUEST_CODE,

        //Game related request codes.
        LEAVE_GAME_REQUEST = 131, GET_QUESTION_REQUEST,
        SUBMIT_ANSWER_REQUEST, GET_GAME_RESULT_REQUEST
    };

    static class JsonRequestPacketSeserializer
    {
        private const int MESSAGE_LEN_SIZE = 4; //The size of the "length" field in the message (in bytes).
        private const int MESSAGE_CODE_SIZE = 1; //The size of the "code" field in the message (in bytes).

        //This interface is used for all of the requests to inherite from.
        public interface Request
        {
        }

        public struct LoginRequest : Request
        {
            public string username;
            public string password;

            public LoginRequest(string username, string password)
            {
                this.username = username;
                this.password = password;
            }
        }

        public struct SignupRequest : Request
        {
            public string username;
            public string password;
            public string email;

            public SignupRequest(string username, string password, string email)
            {
                this.username = username;
                this.password = password;
                this.email = email;
            }

        }

        public struct GetPlayersInRoomRequest : Request
        {
            public uint roomId;

            public GetPlayersInRoomRequest(uint roomId)
            {
                this.roomId = roomId;
            }

        }

        public struct JoinRoomRequest : Request
        {
            public uint roomId;

            public JoinRoomRequest(uint roomId)
            {
                this.roomId = roomId;
            }
        }

        public struct CreateRoomRequest : Request
        {
            public string roomName;
            public uint maxUsers;
            public uint questionCount;
            public uint answerTimeout;

            public CreateRoomRequest(string roomName, uint maxUsers, uint questionCount, uint answerTimeout)
            {
                this.roomName = roomName;
                this.maxUsers = maxUsers;
                this.questionCount = questionCount;
                this.answerTimeout = answerTimeout;
            }
        }
        public struct SubmitAnswerRequest : Request
        {
            public uint answer;

            public SubmitAnswerRequest(uint answer)
            {
                this.answer = answer;
            }
        }

        /*
        This function serializes a login request.
        Input: username - The user's username.
               password - The user's password.
        Output: The serialized login request.
        */
        public static byte[] serializeLoginRequest(string username, string password)
        {
            LoginRequest request = new(username, password);
            return createRequest(requestCode.LOGIN_REQUEST_CODE, request);
        }

        /*
        This function serializes a signup request.
        Input: username - The user's username.
               password - The user's password.
               email - The user's email address.
        Output: The serialized signup request.
        */
        public static byte[] serializeSignupRequest(string username, string password, string email)
        {
            SignupRequest request = new(username, password, email);
            return createRequest(requestCode.SIGNUP_REQUEST_CODE, request);
        }

        /*
        This function serializes a "get players in room" request.
        Input: roomId - The room ID of the rooms whose players the user requests.
        Output: The serialized getPlayers request.
        */
        public static byte[] serializeGetPlayersRequest(uint roomId)
        {
            GetPlayersInRoomRequest request = new(roomId);
            return createRequest(requestCode.GET_PLAYERS_REQUEST_CODE, request);
        }

        /*
        This function serializes a "join room" request.
        Input: roomId - The room ID of the room to which the user wants to join.
        Output: The serialized joinRoom request.
        */
        public static byte[] serializeJoinRoomRequest(uint roomId)
        {
            JoinRoomRequest request = new(roomId);
            return createRequest(requestCode.JOIN_ROOM_REQUEST_CODE, request);
        }

        /*
        This function serializes a "create room" request.
        Input: roomName - The name of the room.
               maxUsers - The maximum amount of users that can enter the room.
               questionCount - The number of questions in the quiz.
               answerTimeout - The time that the users will have to answer each question.
        Output: The serialized "createRoom" request.
        */
        public static byte[] serializeCreateRoomRequest(string roomName, uint maxUsers, uint questionCount, uint answerTimeout)
        {
            CreateRoomRequest request = new(roomName, maxUsers, questionCount, answerTimeout);
            return createRequest(requestCode.CREATE_ROOM_REQUEST_CODE, request);
        }

        /*
        This function serializes a signout request.
        Input: None.
        Output: The serialized signout request.
        */
        public static byte[] serializeSignoutRequest()
        {
            return createRequest(requestCode.SIGNOUT_REQUEST_CODE);
        }

        /*
        This function serializes a "get rooms" request.
        Input: None.
        Output: The serialized getRooms request.
        */
        public static byte[] serializeGetRoomsRequest()
        {
            return createRequest(requestCode.GET_ROOMS_REQUEST_CODE);
        }

        /*
        This function serializes a "get statistics" request.
        Input: None.
        Output: The serialized getStatistics request.
        */
        public static byte[] serializeGetStatisticsRequest()
        {
            return createRequest(requestCode.GET_STATISTICS_REQUEST_CODE);
        }

        /*
        This function serializes a "LeaveRoom" request.
        Input: None.
        Output: The serialized LeaveRoom request.
        */
        public static byte[] serializeLeaveRoomRequest()
        {
            return createRequest(requestCode.LEAVE_ROOM_REQUEST_CODE);
        }

        /*
        This function serializes a "getRoomState" request.
        Input: None.
        Output: The serialized getRoomState request.
        */
        public static byte[] serializeGetRoomStateRequest()
        {
            return createRequest(requestCode.GET_ROOM_STATE_REQUEST_CODE);
        }

        /*
        This function serializes a "closeRoom" request.
        Input: None.
        Output: The serialized closeRoom request.
        */
        public static byte[] serializeCloseRoomRequest()
        {
            return createRequest(requestCode.CLOSE_ROOM_REQUEST_CODE);
        }
        /*
        This function serializes a "StartGame" request.
        Input: None.
        Output: The serialized StartGame request.
        */
        public static byte[] serializeStartGameRequest()
        {
            return createRequest(requestCode.START_GAME_REQUEST_CODE);
        }       
        /*
        This function serializes a "LeaveGame" request.
        Input: None.
        Output: The serialized LeaveGame request.
        */
        public static byte[] serializeLeaveGameRequest()
        {
            return createRequest(requestCode.LEAVE_GAME_REQUEST);
        }
        /*
        This function serializes a "GetQuestion" request.
        Input: None.
        Output: The serialized GetQuestion request.
        */
        public static byte[] serializGetQuestionRequest()
        {
            return createRequest(requestCode.GET_QUESTION_REQUEST);
        }
        /*
        This function serializes a "SubmitAnswer" request.
        Input: answer - The index of the answer.
        Output: The serialized SubmitAnswer request.
        */
        public static byte[] serializSubmitAnswerRequest(uint answer)
        {
            SubmitAnswerRequest request = new SubmitAnswerRequest(answer);
            return createRequest(requestCode.SUBMIT_ANSWER_REQUEST, request);
        }
        /*
        This function serializes a "GetGameResult" request.
        Input: None.
        Output: The serialized GetGameResult request.
        */
        public static byte[] serializGetGameResultRequest()
        {
            return createRequest(requestCode.GET_GAME_RESULT_REQUEST);
        }

        /*
        This function serlializes a request based on its type.
        Input: code - The request code.
               request - The request to be initialized (default = none).
        Output: The serialized request.
        */
        private static byte[] createRequest(requestCode code, Request request = null)
        {
            MemoryStream memStream = new(); //The memory stream that contains the entire message.
            memStream.Write(intToBytes((int)code, MESSAGE_CODE_SIZE));
            if (request != null)
            {
                    string json = JsonConvert.SerializeObject(request, Formatting.Indented); 
                    memStream.WriteAsync(intToBytes(json.Length, MESSAGE_LEN_SIZE));
                    memStream.WriteAsync(Encoding.ASCII.GetBytes(json));
                    var c = memStream.ToArray();
            }

            else //Messages with no data.
            {
                memStream.WriteAsync(intToBytes(0, MESSAGE_LEN_SIZE));
            }
            return memStream.ToArray();
        }

        /*
        This function converts an integer to an array of bytes.
        Input: n - The integer to be converted.
               sizeBytes - The number of bytes that will represent the decimal number.
        Output: An array of bytes that represents the integer in binary.
        */
        private static byte[] intToBytes(int n, int sizeBytes)
        {
            byte[] bytes = BitConverter.GetBytes(n);
            bytes = new ArraySegment<byte>(bytes, 0, sizeBytes).ToArray();
            Array.Reverse(bytes);
            return bytes;
        }
    }
}
