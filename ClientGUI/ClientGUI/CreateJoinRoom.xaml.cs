﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for CreateJoinRoom.xaml
    /// </summary>
    public partial class CreateJoinRoom : Window
    {
        public CreateJoinRoom(bool isJoining)
        {
            InitializeComponent();
            
            if (isJoining)
            {
                DataContext = new AvailableRoomsView();
            } 
            else DataContext = new CreateRoomView();
        }

        /*
        * handle when window closes 
        */
        private void closed_Handler(object sender, EventArgs e)
        {
            DataContext = null;
            this.Owner.Show();
        }
    }
}
