﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for HighScoreView.xaml
    /// </summary>
    public partial class HighScoreView : UserControl
    {
        public HighScoreView()
        {
            InitializeComponent();
            getStatistics();
        }

        /*
        * gets the information from the server
        */
        private void getStatistics()
        {
            Communicator.send(JsonRequestPacketSeserializer.serializeGetStatisticsRequest());
            var answer = Communicator.recive();
            if (answer.code == (int)ResponseCodes.ERROR_CODE)
            {
                var error = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.ErrorResponse>(answer.buffer.ToArray());
                errorText.Text = error.message;
                errorText.Visibility = Visibility.Visible;
            }            
            else if (answer.code == (int)ResponseCodes.SUCCESS_CODE)
            {
                JsonResponsePacketDeserializer.GetStatisticsResponse response = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.GetStatisticsResponse>(answer.buffer.ToArray());
                Tuple<TextBlock, TextBlock, TextBlock>[] scores = {
                    new Tuple<TextBlock, TextBlock, TextBlock>(firstPlace, player1Name, player1Points),
                    new Tuple<TextBlock, TextBlock, TextBlock>(secondPlace, player2Name, player2Points),
                    new Tuple<TextBlock, TextBlock, TextBlock>(thirdPlace, player3Name, player3Points),
                    new Tuple<TextBlock, TextBlock, TextBlock>(fourthPlace, player4Name, player4Points),
                    new Tuple<TextBlock, TextBlock, TextBlock>(fifthPlace, player5Name, player5Points)
                };

                if (response.HighScores.Count > 0)
                {
                    var highScores = response.HighScores.ToList();
                    highScores.Sort((pair1, pair2) => pair2.Value.CompareTo(pair1.Value));

                    for (int i = 0; i < response.HighScores.Count; i++)
                    {
                        scores[i].Item1.Visibility = System.Windows.Visibility.Visible;
                        scores[i].Item2.Visibility = System.Windows.Visibility.Visible;
                        scores[i].Item3.Visibility = System.Windows.Visibility.Visible;
                        scores[i].Item2.Text = highScores[i].Key;
                        scores[i].Item3.Text = highScores[i].Value.ToString();
                    }
                }

                else 
                {
                    errorText.Text = "No scores have been submitted yet.";
                    errorText.Visibility = Visibility.Visible;
                }
                
            }
        }
    }
}
