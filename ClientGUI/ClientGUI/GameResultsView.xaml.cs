﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for GameResultsView.xaml
    /// </summary>

    public partial class GameResultsView : UserControl
    {
        System.Windows.Threading.DispatcherTimer _dispatcherTimerLeaderboard;
        List<PlayerResult> _results;

        /*
        A constructor function. This function creates a "GameResultView" type object.
        Input: None.
        Output: None.
        */
        public GameResultsView()
        {
            InitializeComponent();
            this.Loaded += UserControl1_Loaded;
            _dispatcherTimerLeaderboard = new();
            _dispatcherTimerLeaderboard.Tick += new EventHandler(dispatcherTimerLeaderboard_Tick);
            _dispatcherTimerLeaderboard.Interval = new TimeSpan(0, 0, 3);
            dispatcherTimerLeaderboard_Tick(null, null);
            if (_results == null) _dispatcherTimerLeaderboard.Start();
        }

        /*
        This function receives the game results from the server.
        Input: None.
        Output: None.
        */
        private void getResults()
        {
            Communicator.send(JsonRequestPacketSeserializer.serializGetGameResultRequest());
            Communicator.unserializedResponse response = Communicator.recive();
            if (response.code != (int)ResponseCodes.ERROR_CODE)
            {
                var answer = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.GetGameResultResponse>(response.buffer.ToArray());
                _results = answer.players;
                if (_results == null) //The game has not ended.
                {
                    pleaseWaitText.Visibility = Visibility.Visible;
                    playerStatisticsVisibility(Visibility.Hidden);
                }
                else
                {
                    pleaseWaitText.Visibility = Visibility.Hidden;
                    _results.Sort(delegate (PlayerResult p1, PlayerResult p2) { return p2.pointsScored.CompareTo(p1.pointsScored); });
                    for (int i = 0; i < _results.Count(); i++)
                    {
                        TextBlock textblock = new TextBlock();
                        textblock.Text = Convert.ToString(i + 1) + ") " + _results[i].username + " - " + Convert.ToString(_results[i].pointsScored) + " pts.";
                        leaderboard.Items.Add(textblock);
                    }
                    leaderboardHeader.Visibility = Visibility.Visible;
                    playerStatisticsHeader.Visibility = Visibility.Visible;
                    leaderboard.Visibility = Visibility.Visible;
                    _dispatcherTimerLeaderboard.Stop();
                }
            }
            else
            {
                var answer = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.ErrorResponse>(response.buffer.ToArray());
                MessageBox.Show(answer.message);
            }
        }

        /*
        Handles selection changed in the leaderboard.
        */
        private void leaderboard_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (leaderboard.SelectedItem != null)
            {
                string leaderboardSelection = ((TextBlock)leaderboard.SelectedItem).Text;
                int startIndex = leaderboardSelection.IndexOf(") ") + ") ".Length;
                int nameLength = leaderboardSelection.IndexOf(" - ") - startIndex;
                string player = leaderboardSelection.Substring(startIndex, nameLength);
                int playerIndex = getIndexOfUser(player);

                playerStatisticsVisibility(Visibility.Visible);
                playerStatisticsValueCorrectAnswer.Text = Convert.ToString(_results[playerIndex].correctAnswersCount);
                playerStatisticsValueWorngAnswer.Text = Convert.ToString(_results[playerIndex].wrongAnswerCount);
                playerStatisticsValueAvgTime.Text = Convert.ToString(_results[playerIndex].averageAnswerTime);
            }
        }

        private void playerStatisticsVisibility(Visibility visibility)
        {
            playerStatisticsHeadersCorrectAnswer.Visibility = visibility;
            playerStatisticsHeadersWorngAnswer.Visibility = visibility;
            playerStatisticsHeadersAvgTime.Visibility = visibility;
            playerStatisticsValueCorrectAnswer.Visibility = visibility;
            playerStatisticsValueWorngAnswer.Visibility = visibility;
            playerStatisticsValueAvgTime.Visibility = visibility;
        }

        /*
        This function returns the index of a players's results data in the results list that was recieved from the server.
        Input: username - The player's username.
        Output: The index of the user's data in the list "_results".
        */
        private int getIndexOfUser(string username)
        {
            for (int i = 0; i < _results.Count; i++)
            {
                if (_results[i].username == username)
                {
                    return i;
                }
            }
            return 0;
        }

        /*
        Handles the tick of the DispatchTimer "_dispatcherTimerLeaderboard".
        */
        private void dispatcherTimerLeaderboard_Tick(object sender, EventArgs e)
        {
            getResults();
        }

        /*
        Handles the click event of the "Back to Main Menu" button. 
        */
        private void mainMenuButton_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }

        /*
         handles when window closes.
        */
        private void closed_Handler(object sender, EventArgs e)
        {
            _dispatcherTimerLeaderboard.Stop();
        }

        /*
        * adds an event handler to the closing events of the window.
        */
        void UserControl1_Loaded(object sender, RoutedEventArgs e)
        {
            Window window = Window.GetWindow(this);
            window.Title = "Game Results";
            window.Closing += closed_Handler;
        }
    }
}
