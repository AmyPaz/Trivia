﻿using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientGUI
{
    public enum ResponseCodes { ERROR_CODE = 254, SUCCESS_CODE };
    public struct RoomData
    {
        public uint id;
        public string name;
        public uint maxPlayers;
        public uint numOfQuestionInGame;
        public uint timePerQuestion;
        public uint isActive;
    }
    public struct PlayerResult
    {
        public uint correctAnswersCount;
        public float averageAnswerTime;
        public string username;
        public uint pointsScored;
        public uint wrongAnswerCount;
    }
    class JsonResponsePacketDeserializer
    {
        public struct ErrorResponse
        {
           public string message;
        }
        public struct LoginResponse
        {
            public uint status;
        }
        public struct SignupResponse
        {
            public uint status;
        }
        public struct LogoutResponse
        {
            public uint status;
        }
        public struct GetRoomsResponse
        {
            public uint status;
            public Dictionary<string, RoomData> Rooms;
        }
        public struct GetPlayersInRoomResponse
        {
            public uint status;
            public List<string> PlayersInRoom;
        }
        public struct GetStatisticsResponse
        {
            public uint status;
            public Dictionary<string, int> UserStatistics;
            public Dictionary<string, int> HighScores;
        }
        public struct JoinRoomResponse
        {
            public uint status;
        }
        public struct CloseRoomResponse
        {
            public uint status;
        }
        public struct CreateRoomResponse
        {
            public uint status;
            public uint roomId;
        }
        public struct GetRoomStateResponse
        {
            public uint status;
            public bool hasGameBegun;
            public List<string> players;
            public uint answerCount;
            public uint answerTimeout;
        }
        public struct GetGameResultResponse
        {
            public uint status;
            public List<PlayerResult> players;
        }
        public struct SubmitAnswerResponse
        {
            public uint status;
            public uint correctAnswerId;
        }
        public struct LeaveGameResponse
        {
            public uint status;
        }
        public struct StartGameResponse
        {
            public uint status;
        }
        public struct GetQuestionResponse
        {
            public uint status;
            public string question;
            public List<string> answers;
        }

        public static T responseDeserialize<T>(byte[] buffer)
        {
            MemoryStream ms = new(buffer);
            using (BsonReader reader = new(ms))
            {
                JsonSerializer serializer = new();
                return serializer.Deserialize<T>(reader);
            }
        }
       
    }
}