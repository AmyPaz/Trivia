﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for CreateRoomView.xaml
    /// </summary>
    public partial class CreateRoomView : UserControl
    {
        public CreateRoomView()
        {
            InitializeComponent();
        }

        /*
         * event handler for the Join Room button insted of creating one
         */
        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).DataContext = new AvailableRoomsView();
        }

        /*
         * event handler for the back button
         */
        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Owner.Show();
            Window.GetWindow(this).Close();
        }

        /*
        This function handles the event when enter is pressed in the form.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Ouput: None.
        */
        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                createRoomButton_Click(null, null);
            }
        }

        /*
         * event handler. creates the room and shows th waiting room
         */
        private void createRoomButton_Click(object sender, RoutedEventArgs e)
        {
            if (roomName.Text == "" || numberOfQuestions.Text == "" || questionTime.Text == "" || maxPlayers.Text == "")
            {
                errorText.Text = "Please make sure to fill all of the fields above.";
                errorText.Visibility = Visibility.Visible;
                return;
            }

            string roomNameText = roomName.Text;
            uint amountOfQuestions = Convert.ToUInt32(numberOfQuestions.Text);
            uint timePerQustion = Convert.ToUInt32(questionTime.Text);
            uint maxPlayersNum = Convert.ToUInt32(maxPlayers.Text);
            
            var request = JsonRequestPacketSeserializer.serializeCreateRoomRequest(roomNameText, maxPlayersNum, amountOfQuestions, timePerQustion);
            Communicator.send(request);
            var answer = Communicator.recive();
            if (answer.code == (int)ResponseCodes.ERROR_CODE)
            {
                var error = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.ErrorResponse>(answer.buffer.ToArray());
                errorText.Text = error.message;
                errorText.Visibility = Visibility.Visible;
            }
            else if (answer.code == (int)ResponseCodes.SUCCESS_CODE)
            {
                var response = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.CreateRoomResponse>(answer.buffer.ToArray());
                Window.GetWindow(this).Title = "Waiting Room";
                Window.GetWindow(this).DataContext = new WaitingRoomView(response.roomId, roomNameText, true);                
            }
        }

        /*
         * validation for text box to accept only numbers
         */
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
