﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for AvailableRoomsView.xaml
    /// </summary>
    public partial class AvailableRoomsView : UserControl
    {
        private Dictionary<string, RoomData> _rooms; //The rooms that were reveived from the server.
        System.Windows.Threading.DispatcherTimer dispatcherTimerRooms;
        System.Windows.Threading.DispatcherTimer dispatcherTimerUsers;
        private const int ITEM_NOT_SELECTED = -1; //The index of the listbox when an item is not selected.
        private const int ITEM_NOT_IN_LIST = -1; //The value that the function "getItemIndex" returns if the item is not in the list.

        /*
        A constructor function. This function creates a new "JoinRoom" window.
        Input: None.
        Output: None.
        */
        public AvailableRoomsView()
        {
            InitializeComponent();
            getRoomsInfo();
            //  dispatcherTimerUsers setup
            dispatcherTimerUsers = new();
            dispatcherTimerUsers.Tick += new EventHandler(dispatcherTimerUsers_Tick);
            dispatcherTimerUsers.Interval = new TimeSpan(0, 0, 3);
            //  dispatcherTimerRooms setup
            dispatcherTimerRooms = new();
            dispatcherTimerRooms.Tick += new EventHandler(dispatcherTimerRooms_Tick);
            dispatcherTimerRooms.Interval = new TimeSpan(0, 0, 3);
            dispatcherTimerRooms.Start();
            this.Loaded += UserControl1_Loaded;
        }

        /*
        This function receives the available rooms from the server and displays them.
        Input: None.
        Output: None.
        */
        void getRoomsInfo()
        {
            Communicator.send(JsonRequestPacketSeserializer.serializeGetRoomsRequest());
            Communicator.unserializedResponse response = Communicator.recive();
            if (response.code != (int)ResponseCodes.ERROR_CODE)
            {
                _rooms = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.GetRoomsResponse>(response.buffer.ToArray()).Rooms;

                //If there are available room for the client to join to.
                if (_rooms != null)
                {
                    //Adds rooms to the listbox if they are not in it already.
                    foreach (var room in _rooms)
                    {
                        TextBlock text = new TextBlock();
                        text.Text = room.Value.name;
                        if (getItemIndex(text, listOfRooms) == ITEM_NOT_IN_LIST)
                            listOfRooms.Items.Add(text);
                    }
                    joinButton.IsEnabled = true;
                    listOfRooms.Visibility = Visibility.Visible;
                    listOfUsers.Visibility = Visibility.Visible;
                    availableRoomsHeader.Visibility = Visibility.Visible;
                    usersInRoomHeader.Visibility = Visibility.Visible;
                    errorText.Visibility = Visibility.Hidden;
                    if (listOfRooms.SelectedIndex != ITEM_NOT_SELECTED)
                    {
                        roomDataHeaders.Visibility = Visibility.Visible;
                        roomData.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        roomDataHeaders.Visibility = Visibility.Hidden;
                        roomData.Visibility = Visibility.Hidden;
                    }
                }
                else
                {
                    listOfRooms.Visibility = Visibility.Hidden;
                    listOfRooms.Items.Clear();
                    listOfUsers.Visibility = Visibility.Hidden;
                    listOfUsers.Items.Clear();
                    availableRoomsHeader.Visibility = Visibility.Hidden;
                    usersInRoomHeader.Visibility = Visibility.Hidden;
                    joinButton.IsEnabled = false;
                    roomDataHeaders.Visibility = Visibility.Hidden;
                    roomData.Visibility = Visibility.Hidden;
                    errorText.Text = "There are not any available rooms for you to join to.";
                    errorText.Visibility = Visibility.Visible;
                }
            }
            else
            {
                errorText.Text = "Failed to receive the list of available rooms from the server.";
                errorText.Visibility = Visibility.Visible;
            }
        }

        /*
         * event handler when dispatcherTimerRooms Ticks. this functhion refreshes the user list.
         */
        private void dispatcherTimerRooms_Tick(object sender, EventArgs e)
        {
            if (_rooms != null)
            {
                for (int i = 0; i < listOfRooms.Items.Count; i++)
                {
                    if (!_rooms.ContainsKey(((TextBlock)listOfRooms.Items[i]).Text))
                        listOfRooms.Items.RemoveAt(i);
                }
            }
            getRoomsInfo();
        }

        /*
         * event handler when dispatcherTimerUsers Ticks. this functhion refreshes the user list
         */
        private void dispatcherTimerUsers_Tick(object sender, EventArgs e)
        {
            if (listOfRooms.SelectedItem != null && _rooms.ContainsKey(((TextBlock)listOfRooms.SelectedItem).Text) && _rooms != null)
            {
                string roomName = ((TextBlock)listOfRooms.SelectedItem).Text;
                Communicator.send(JsonRequestPacketSeserializer.serializeGetPlayersRequest(_rooms[roomName].id));
                Communicator.unserializedResponse response = Communicator.recive();
                if (response.code != (int)ResponseCodes.ERROR_CODE) //If the room is joinable.
                {
                    //Adds the name of the participants to the ListBox "listOfUsers".
                    var usernames = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.GetPlayersInRoomResponse>(response.buffer.ToArray()).PlayersInRoom;
                    updateUsersInRoom(usernames);
                }
            }
        }

        /*
        This function returns the index of a TextBlock item in a ListBox control.
        Input: findItem - The item whose index in the listbox will be returned.
               listbox - The listbox in which the item is searched.
        Output: The index of the item in the listbox if the item is found, or -1 if it is not.
        */
        private int getItemIndex(TextBlock findItem, ListBox listBox)
        {
            for (int i = 0; i < listBox.Items.Count; i++)
            {
                if (((TextBlock)listBox.Items[i]).Text == findItem.Text) return i;
            }
            return ITEM_NOT_IN_LIST;
        }

        /*
        This function is used to handle the "Selection Changed" event of the "listOfRooms" ListBox.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Output: None.
        */
        private void listOfRooms_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listOfRooms.SelectedItem != null)
            {
                string roomName = ((TextBlock)listOfRooms.SelectedItem).Text;
                dispatcherTimerUsers_Tick(null, null);
                dispatcherTimerUsers.Start();
                //Shows the details regarding the room.
                roomData.Text = _rooms[roomName].numOfQuestionInGame.ToString() + "\n" + _rooms[roomName].timePerQuestion.ToString() + "\n" + _rooms[roomName].maxPlayers.ToString();
                roomData.Visibility = System.Windows.Visibility.Visible;
                roomDataHeaders.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                dispatcherTimerUsers.Stop();
                roomData.Visibility = System.Windows.Visibility.Hidden;
                roomDataHeaders.Visibility = System.Windows.Visibility.Hidden;
            }
        }
        
        /*
        This function adds the names of the users in a room to the listbox of users.
        Input: usernames - A list that contains the names of the users in the room.
        Output: None.
        */
        private void updateUsersInRoom(List<string> usernames)
        {
            listOfUsers.Items.Clear();
            foreach (string username in usernames)
            {
                listOfUsers.Items.Add(username);
            }
        }

        /*
         * handle when window closes 
         */
        private void closed_Handler(object sender, EventArgs e)
        {
            dispatcherTimerRooms.Stop();
            dispatcherTimerUsers.Stop();
        }

        /*
        This function is used to handle the "click" event of the "Back" button.
        Input: sender - The object that alerted of the event.
                e - Arguments related to the event.
        Output: None.
        */
        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }

        /*
        This function is used to handle the "click" event of the "Join Room" button.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Output: None.        
        */
        private void joinButton_Click(object sender, RoutedEventArgs e)
        {
            if (listOfRooms.SelectedItem != null)
            {
                string roomName = ((TextBlock)listOfRooms.SelectedItem).Text;
                uint roomId = _rooms[roomName].id;
                Communicator.send(JsonRequestPacketSeserializer.serializeJoinRoomRequest(roomId));
                Communicator.unserializedResponse response = Communicator.recive();
                if (response.code != (int)ResponseCodes.ERROR_CODE) //The room can be joined to.
                {
                    dispatcherTimerRooms.Stop();
                    dispatcherTimerUsers.Stop();
                    WaitingRoomView waitingRoom = new WaitingRoomView(roomId, roomName, false);
                    Window.GetWindow(this).Title = "Waiting Room";
                    Window.GetWindow(this).DataContext = waitingRoom;
                }

                else 
                {
                    var answer = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.ErrorResponse>(response.buffer.ToArray());
                    errorText.Text = answer.message;
                    errorText.Visibility = Visibility.Visible;
                }
            }

            else
            {
                errorText.Text = "Please select the room that you would like to join to.";
                errorText.Visibility = Visibility.Visible;
            }
        }

        /*
        * adds an event handler to the closing events of the window.
        */
        void UserControl1_Loaded(object sender, RoutedEventArgs e)
        {
            Window window = Window.GetWindow(this);
            window.Closing += closed_Handler;
        }
    }
}

