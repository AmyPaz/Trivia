﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for Statistics.xaml
    /// </summary>
    public partial class Statistics : Window
    {
        PersonalStatisticsView personalStatisticsView = null;
        HighScoreView highScoreView = null;
        public Statistics()
        {
            InitializeComponent();
        }

        /*event handler for the back button*/
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /*show the personal statistics*/
        private void personalStatisticsButton_Click(object sender, RoutedEventArgs e)
        {
            if(personalStatisticsView == null) personalStatisticsView = new ();
            DataContext = personalStatisticsView;
            header.Visibility = Visibility.Hidden;
        }

        /*show the high score*/
        private void highScoreButton_Click(object sender, RoutedEventArgs e)
        {
            if (highScoreView == null) highScoreView = new ();
            DataContext = highScoreView;
            header.Visibility = Visibility.Hidden;
        }

        /*
         * handle when window closes 
         */
        private void closed_Handler(object sender, EventArgs e)
        {
            this.Owner.Show();
        }
    }
}
