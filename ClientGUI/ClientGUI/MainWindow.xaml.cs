﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Communicator.connect();
            Application.Current.Properties["isLoggedin"] = false;
        }

        /*
        This function handles the event "click" of the "Signup" button.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Ouput: None.
        */
        private void signupButton_Click(object sender, RoutedEventArgs e)
        {
            Signup signup = new Signup();
            signup.Show();
            signup.Owner = this;
            this.Hide();
        }

        /*
        This function handles the event "click" of the "Create Room" button.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Ouput: None.
        */
        private void createRoomButton_Click(object sender, RoutedEventArgs e)
        {
            CreateJoinRoom createRoom = new CreateJoinRoom(false);
            createRoom.Title = "Create room";
            createRoom.Show();
            createRoom.Owner = this;
            this.Hide();
        }

        /*
        This function handles the event "click" of the "Personal Statistics" button.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Ouput: None.
        */
        private void statisticsButton_Click(object sender, RoutedEventArgs e)
        {
            Statistics statistics = new Statistics();
            statistics.Show();
            statistics.Owner = this;
            this.Hide();
        }

        /*
        This function handles the event "click" of the "Join Room" button.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Ouput: None.
        */
        private void joinRoomButton_Click(object sender, RoutedEventArgs e)
        {
            CreateJoinRoom createRoom = new CreateJoinRoom(true);
            createRoom.Title = "Join room";
            createRoom.Owner = this;
            createRoom.Show();
            this.Hide();
        }

        /*
        This function handles the event "click" of the "Log in" button.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Ouput: None.
        */
        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            Login loginWindow = new Login();
            loginWindow.Show();
            loginWindow.Owner = this;
            this.Hide();
        }

        /*
        This function handles the event "click" of the "Quit" button.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Ouput: None.
        */
        private void quitButton_Click(object sender, RoutedEventArgs e)
        {
            //If the user is logged in at the moment.
            if ((bool)Application.Current.Properties["isLoggedin"])
            {
                Communicator.send(JsonRequestPacketSeserializer.serializeSignoutRequest());
                if (Communicator.recive().code == (int)ResponseCodes.ERROR_CODE)
                {
                    MessageBox.Show("Failed to log out",
                        "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                else
                {
                    loginButton.IsEnabled = true;
                    signupButton.IsEnabled = true;
                    this.Resources["isLoggedin"] = false;
                    Application.Current.Properties["isLoggedin"] = false;
                }
            }

            else
            {
                this.Close();
            }
        }

        /*
        This function handles the event "Window Closed" of the window.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Ouput: None.
        */
        private void Window_Closed(object sender, EventArgs e)
        {
            Communicator.closeConnection();
        }
    }
}
