﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for Signup.xaml
    /// </summary>
    public partial class Signup : Window
    {
        public Signup()
        {
            InitializeComponent();
        }

        /* event handler for signin button text*/
        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Login login = new Login();
            login.Show();
            login.Owner = this.Owner;
            this.Close();
            login.Owner.Hide();
        }


        /*event handler for the back button*/
        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /*
        This function handles the event when enter is pressed in the form.
        Input: sender - The object that alerted of the event.
               e - Arguments related to the event.
        Ouput: None.
        */
        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                signupButton_Click(null, null);
            }
        }

        /* event handler for signup. signup the user at the server*/
        private void signupButton_Click(object sender, RoutedEventArgs e)
        {
            string usernameText = username.Text;
            string emailText = email.Text;
            string passwordText = password.Password;
            if (usernameText == "" || emailText == "" || passwordText == "")
            {
                errorText.Text = "Please make sure to fill all of the fields above.";
                errorText.Visibility = Visibility.Visible;
            }
            else
            {
                var request = JsonRequestPacketSeserializer.serializeSignupRequest(usernameText, passwordText, emailText);
                Communicator.send(request);
                Communicator.unserializedResponse response = Communicator.recive();
                if (response.code == (int)ResponseCodes.ERROR_CODE)
                {
                    var answer = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.ErrorResponse>(response.buffer.ToArray());
                    errorText.Text = answer.message;
                    errorText.Visibility = Visibility.Visible;
                }
                else if (response.code == (int)ResponseCodes.SUCCESS_CODE)
                {
                    Application.Current.Properties["isLoggedin"] = true;
                    Application.Current.MainWindow.Resources["isLoggedin"] = true;
                    ((MainWindow)Application.Current.MainWindow).loginButton.IsEnabled = false;
                    ((MainWindow)Application.Current.MainWindow).signupButton.IsEnabled = false;
                    this.Close();
                }
            }
        }

        /*
         * handle when window closes
         * shows the main window
         */
        private void closed_Handler(object sender, EventArgs e)
        {
            this.Owner.Show();
        }
    }
}
