﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for questionView.xaml
    /// </summary>
    public partial class questionView : UserControl
    {
        private uint _timePerQuestion; //The time that the player has to answer each question. 
        private uint _numberOfQuestions; //The number of questions in the game.
        private uint _questionsAnsweredCorrectly; //The number of correct answers that the user submitted.
        private uint _questionsAnswered; //The number of questions that the user answered.
        private const int DELAY_TIME = 1000; //The time in millisecondes to wait between questions.
        private DispatcherTimer _timer; 
        private TimeSpan _time;
        private List<String> _answers; //The answers to the current questions.

        /*
        A constructor function. This function creates a new "questionView" type object.
        Input: timePerQuestion - The time that the user will have to answer each question.
               numberOfQuestions - The number of questions that the user will have to answer.
        Output: None.
        */
        public questionView(uint timePerQuestion, uint numberOfQuestions)
        {
            InitializeComponent();
            _questionsAnsweredCorrectly = 0;
            _questionsAnswered = 0;
            _numberOfQuestions = numberOfQuestions;
            _timePerQuestion = timePerQuestion;
            _timer = new();
            _timer.Tick += new EventHandler(dispatcherTimer_Tick);
            _timer.Interval = new TimeSpan(0, 0, 1);
            remainingQuestions.Text = Convert.ToString(_numberOfQuestions);
            this.Loaded += UserControl1_Loaded;

        }

        /*
        This function receives a question from the server and displays it on the screen.
        Input: None.
        Output: None.
        */
        public void getQuestion()
        {
            remainingQuestions.Text = Convert.ToString(_numberOfQuestions - _questionsAnswered - 1);
            changeButtonsMode(true, Brushes.Transparent);
            Communicator.send(JsonRequestPacketSeserializer.serializGetQuestionRequest());
            Communicator.unserializedResponse response = Communicator.recive();
            if (response.code != (int)ResponseCodes.ERROR_CODE)
            {
                var answer = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.GetQuestionResponse>(response.buffer.ToArray());
                question.Text = answer.question;
                Window.GetWindow(this).Title = "Question " + Convert.ToString(_questionsAnswered + 1);

                _answers = answer.answers;
                String[] answersArray = answer.answers.ToArray();
                shuffleArray<String>(answersArray);
                int i = 0; 

                ((TextBlock)ans1.Content).Text = answersArray[i++];
                ((TextBlock)ans2.Content).Text = answersArray[i++];
                if (answer.answers.Count() > 2)
                {
                    ((TextBlock)ans3.Content).Text = answersArray[i++];
                    ans3.Visibility = Visibility.Visible;
                    if (answer.answers.Count() > 3)
                    {
                        ((TextBlock)ans4.Content).Text = answersArray[i++];
                        ans4.Visibility = Visibility.Visible;
                    }
                    else ans4.Visibility = Visibility.Hidden;
                }
                else
                {
                    ans3.Visibility = Visibility.Hidden;
                    ans4.Visibility = Visibility.Hidden;
                }
                _time = TimeSpan.FromSeconds(_timePerQuestion);
                dispatcherTimer_Tick(null, null);
                _timer.Start();
            }
            else
            {
                var answer = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.ErrorResponse>(response.buffer.ToArray());
                MessageBox.Show(answer.message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                Window.GetWindow(this).Close();
            }
        }

        /*
        This function sends the user's answer to the server.
        Input: The user's answer.
        Output: The index of the correct answer in the list _answers.
        */
        private int submitAnswer(string answer)
        {
            Communicator.send(JsonRequestPacketSeserializer.serializSubmitAnswerRequest((uint)(_answers.IndexOf(answer))));
            Communicator.unserializedResponse response = Communicator.recive();
            int correctAnswerId = 0;
            if (response.code != (int)ResponseCodes.ERROR_CODE)
            {
                _questionsAnswered++;
                var content = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.SubmitAnswerResponse>(response.buffer.ToArray());
                correctAnswerId = (int)content.correctAnswerId;
            }
            else
            {
                var content = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.ErrorResponse>(response.buffer.ToArray());
                MessageBox.Show(content.message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                Window.GetWindow(this).Close();
            }
            return correctAnswerId;
        }

        /*
        This function handles the situation when the user has not submitted his answer in time.
        Input: None.
        Output: None.
        */
        private void timeout()
        {
            _timer.Stop();
            int correctAnswer = submitAnswer(_answers[0]);
            for (int i = 0; i < answersStackPanel.Children.Count; i++)
            {
                ((Button)answersStackPanel.Children[i]).IsHitTestVisible = false;
                ((Button)answersStackPanel.Children[i]).Background = Brushes.IndianRed;
            }
            markCorrectAnswer(correctAnswer);
            nextQuestionAsync();
        }

        /*
        Handles the tick event of the DispatcherTimer "_timer".
        */
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (_time == TimeSpan.Zero) timeout();
            tbTime.Text = _time.ToString("c");
            _time = _time.Add(TimeSpan.FromSeconds(-1));
        }

        /*
        Handles the click event of the answer buttons. 
        */
        private void ans_Click(object sender, RoutedEventArgs e)
        {
            changeButtonsMode(false);
            _timer.Stop();
            int correctAnswerId = submitAnswer(((TextBlock)(((Button)sender).Content)).Text);
            if (_answers[correctAnswerId] == ((TextBlock)(((Button)sender).Content)).Text)
            {
                ((Button)sender).Background = Brushes.LimeGreen;
                _questionsAnsweredCorrectly++;
                questionsAnsweredCorrectly.Text = Convert.ToString(_questionsAnsweredCorrectly);
            }
            else
            {
                ((Button)sender).Background = Brushes.IndianRed;
                markCorrectAnswer(correctAnswerId);
            }
            nextQuestionAsync();
        }

        /*
        This function marks the correct answer to the current question.
        Input: correctAnswerId - The index of the correct answer in the list "_answers".
        Output: None.
        */
        private void markCorrectAnswer(int correctAnswerId)
        {
            for (int i = 0; i < answersStackPanel.Children.Count; i++)
            {
                if (((TextBlock)(((Button)answersStackPanel.Children[i]).Content)).Text == _answers[correctAnswerId])
                {
                    ((Button)answersStackPanel.Children[i]).Background = Brushes.LimeGreen;
                    return;
                }
            }
        }

        /*
         handles when window closes.
        */
        private void closed_Handler(object sender, EventArgs e)
        {
            _timer.Stop();
        }

        /*
        * adds an event handler to the closing events of the window.
        */
        void UserControl1_Loaded(object sender, RoutedEventArgs e)
        {
            Window window = Window.GetWindow(this);
            window.Closing += closed_Handler;
            getQuestion();
        }

        /*
        This function progresses the user to the next questions. If the game ended, it will move the user to the results screen.
        */
        private async void nextQuestionAsync()
        {
            await Task.Delay(DELAY_TIME);
            if (_questionsAnswered < _numberOfQuestions)
            {
                getQuestion();
            }
            else
            {
                GameResultsView gameResults = new GameResultsView();
                Window.GetWindow(this).DataContext = gameResults;
            }
        }

        /*
        This function randomly shuffles an array.
        Input: arr - The array that will be shuffled.
        Output: None.
        */
        private void shuffleArray<T>(T[] arr)
        {
            Random rng = new Random();
            int n = arr.Length;
            while (n > 1)
            {
                int k = rng.Next(n--);
                T temp = arr[n];
                arr[n] = arr[k];
                arr[k] = temp;
            }
        }

        /*
        Handles the click event of the "Leave Room" button. 
        */
        private void leaveGameButton_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }

        /*
         * Change enabeld and background for all buttons
         */
        private void changeButtonsMode(bool isDisabled, Brush background = null)
        {
            for (int i = 0; i < answersStackPanel.Children.Count; i++)
            {
                ((Button)answersStackPanel.Children[i]).IsHitTestVisible = isDisabled;
                if(background != null)
                    ((Button)answersStackPanel.Children[i]).Background = background;
            }
        }
    }
}
