﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientGUI
{
    /// <summary>
    /// Interaction logic for WaitingRoomView.xaml
    /// </summary>
    public partial class WaitingRoomView : UserControl
    {
        private System.Windows.Threading.DispatcherTimer dispatcherTimer;
        private uint _roomId;
        private string _roomName;
        private uint _timePerQuestion;
        private uint _numQuestion;
        private bool _hasGameBegun;
        private List<string> _players;
        private bool _isAdmin;

        public WaitingRoomView(uint roomId, string roomName, bool isAdmin)
        {
            InitializeComponent();
            this._roomId = roomId;
            this._roomName = roomName;
            this._isAdmin = isAdmin;
            roomNameHeader.Text = _roomName;
            if (isAdmin)
            {
                startGameButton.Visibility = Visibility.Visible;
                closeRoomButton.Visibility = Visibility.Visible;
                simplePlayerText.Visibility = Visibility.Hidden;
                leaveRoomButton.Visibility = Visibility.Hidden;
            }
            else
            {
                startGameButton.Visibility = Visibility.Hidden;
                closeRoomButton.Visibility = Visibility.Hidden;
                simplePlayerText.Visibility = Visibility.Visible;
                leaveRoomButton.Visibility = Visibility.Visible;
            }

            //  DispatcherTimer setup
            dispatcherTimer = new();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 3);
            this.Loaded += UserControl1_Loaded;
        }

        /*
        This function receives the room info from the server.
        Input: None.
        Output: None.
        */
        void getRoomInfo()
        {
            Communicator.send(JsonRequestPacketSeserializer.serializeGetRoomStateRequest());
            Communicator.unserializedResponse response = Communicator.recive();
            if (response.code != (int)ResponseCodes.ERROR_CODE) 
            {
                var answer = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.GetRoomStateResponse>(response.buffer.ToArray());
                _numQuestion = answer.answerCount;
                _timePerQuestion = answer.answerTimeout;
                _hasGameBegun = answer.hasGameBegun;
                _players = answer.players;
            }
            else
            {
                var answer = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.ErrorResponse>(response.buffer.ToArray());
                MessageBox.Show(answer.message, "Leave the Room", MessageBoxButton.OK, MessageBoxImage.Information);
                Window.GetWindow(this).Close();
            }
        }

        /*
         * event handler when dispatcherTimer Ticks. this functhion refreshes the user list
         */
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            getRoomInfo();
            if (_hasGameBegun)
            {
                dispatcherTimer.Stop();
                Window game = new Game(_timePerQuestion, _numQuestion);
                game.Owner = Window.GetWindow(this).Owner;
                Window.GetWindow(this).Close();
                game.Show();
                game.Owner.Hide();
            }
            else
            {
                TextBlock participant;
                roomParticipent.Children.Clear();
                numberOfQuestionsHeader.Text = "Number of Questions: " + Convert.ToString(_numQuestion);
                timePerQuestionHeader.Text = "Time per Question (Sec): " + Convert.ToString(_timePerQuestion);
                admin.Text = "Admin: " + _players[0];
                foreach (string player in _players)
                {
                    participant = new TextBlock();
                    participant.HorizontalAlignment = HorizontalAlignment.Center;
                    participant.FontSize = 17;
                    participant.Text = player;
                    participant.FontWeight = FontWeights.SemiBold;
                    roomParticipent.Children.Add(participant);
                }
            }
        }

        /*
        This function closes the room.
        Input: None.
        Output: None.
        */
        private void closeRoom()
        {
            Communicator.send(JsonRequestPacketSeserializer.serializeCloseRoomRequest());
            Communicator.unserializedResponse response = Communicator.recive();
            if (response.code == (int)ResponseCodes.ERROR_CODE)
            {
                var answer = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.ErrorResponse>(response.buffer.ToArray());
                MessageBox.Show(answer.message);
            }
        }
        
        /*
        This function removes a non-admin user from the room. 
        Input: None.
        Output: None.
        */
        private void leaveRoom()
        {
            Communicator.send(JsonRequestPacketSeserializer.serializeLeaveRoomRequest());
            Communicator.unserializedResponse response = Communicator.recive();
            if (response.code == (int)ResponseCodes.ERROR_CODE)
            {
                var answer = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.ErrorResponse>(response.buffer.ToArray());
                MessageBox.Show(answer.message);
            }
        }

        /*
         * add an event handler to the closing events of the window.
         */
        void UserControl1_Loaded(object sender, RoutedEventArgs e)
        {
            Window window = Window.GetWindow(this);
            window.Closing += window_Closing;
            dispatcherTimer_Tick(null, null);
            if(!_hasGameBegun) dispatcherTimer.Start();
        }

        /*
         * handels events when room is closed by the X button
         */
        void window_Closing(object sender, global::System.ComponentModel.CancelEventArgs e)
        {
            dispatcherTimer.Stop();
            if (!_hasGameBegun)
            {
                if (_isAdmin) closeRoom();
                else leaveRoom();
            }
        }

        /*
         * handles the leave room button click event.
         */
        private void leaveRoomButton_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }

        /*
         * handles the close room button click event.
         */
        private void closeRoomButton_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }

        /*
         * event handler to the play game button click event.
         */
        private void startGameButton_Click(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Stop();
            Communicator.send(JsonRequestPacketSeserializer.serializeStartGameRequest());
            Communicator.unserializedResponse response = Communicator.recive();
            if (response.code == (int)ResponseCodes.ERROR_CODE)
            { 
                var answer = JsonResponsePacketDeserializer.responseDeserialize<JsonResponsePacketDeserializer.ErrorResponse>(response.buffer.ToArray());
                MessageBox.Show(answer.message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                Window.GetWindow(this).Close();
            }
            _hasGameBegun = true;
            Window game = new Game(_timePerQuestion, _numQuestion);
            game.Owner = Window.GetWindow(this).Owner;
            Window.GetWindow(this).Close();
            game.Show();
            game.Owner.Hide();
        }
    }
}
