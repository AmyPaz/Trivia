import socket
import json
from bitarray import bitarray
from bitarray.util import ba2int
import bson
from sys import exit
from math import ceil

SERVER_IP = "127.0.0.1"
SERVER_PORT = 8853

# Different codes that the server/client send based on the request/response.
LOGIN_REQUEST_CODE = 101
SIGNUP_REQUEST_CODE = 102
ERROR_CODE = 254
SUCCESS_CODE = 255

BYTE_SIZE = 8

# The size of the code field in the request.
REQUEST_CODE_SIZE = 1

# The size of the content length field in the request.
MESSAGE_LEN_SIZE = 4


def connect_to_server(server_ip, server_port):
    """
    This function connects the client to the server.
    :param server_ip: The server's ip.
    :type server_ip: string.
    :param server_port: The port on which the server receives new clients.
    :type server_port: int.
    :return: The socket with the server.
    :rtype: socket.
    """
    server_address = (server_ip, server_port)
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except Exception as e:
        print("Failed to create a socket. Error message: ", e)
        exit(1)

    try:
        sock.connect(server_address)
    except Exception as e:
        print("Failed to connect to the server. Error message: ", e)
        exit(1)
    return sock


def int_to_bin(n):
    """
    This function convert a decimal number to a binary number.
    :param n: The decimal number.
    :type n: int.
    :return: The same number that was given to the number, but in binary.
    :rtype: binary.
    """
    return bin(n)[2:]


def pad_with_zeros(num, num_of_bytes):
    """
    This function pads the beginning of a binary number with zeros.
    :param num: The number that will be padded with zeros.
    :type num: int.
    :param num_of_bytes: The final size (in bytes) of the given number.
    :type num_of_bytes: binary.
    :return: The padded number.
    :rtype: binary.
    """
    padding = bitarray(BYTE_SIZE * num_of_bytes - len(num))
    padding.setall(0)
    return padding + num


def convert_int_to_bitarray(n):
    """
    This function returns a bitarray representation of a decimal number.
    :param n: The decimal number.
    :type n: int.
    :return: The bitarray representation of the given number.
    :rtype: bitarray.
    """

    return bitarray(int_to_bin(n))


def send_signup_message(sock, username, password, email_address):
    """
    This function sends a signup message to the server.
    :param sock: The socket with the server.
    :type sock: socket.
    :param username: The user's username.
    :type username: string.
    :param password: The user's password.
    :type password: string.
    :param email_address: The user's email address.
    :type email_address: string.
    :return: None.
    """
    content = {
        "username": username,
        "password": password,
        "email": email_address
    }
    binary_content = bitarray(endian='big')
    binary_content.frombytes(json.dumps(content).encode())

    request_code = pad_with_zeros(convert_int_to_bitarray(SIGNUP_REQUEST_CODE), REQUEST_CODE_SIZE)
    length = pad_with_zeros(convert_int_to_bitarray(len(json.dumps(content).encode())), MESSAGE_LEN_SIZE)

    message = bitarray(request_code + length + binary_content)
    sock.sendall(message.tobytes())
    response_code = sock_bin_to_int(sock, 1)
    response_size = sock_bin_to_int(sock, 4)
    response = sock.recv(response_size)
    print(bson.loads(response))


def send_login_message(sock, username, password):
    """
    This function sends a login message to the server.
    :param sock: The socket with the server.
    :type sock: socket.
    :param username: The user's username.
    :type username: string.
    :param password: The user's password.
    :type password: string.
    :return: None.
    """
    content = {
        "username": username,
        "password": password
    }
    binary_content = bitarray(endian='big')
    binary_content.frombytes(json.dumps(content).encode())

    request_code = pad_with_zeros(convert_int_to_bitarray(LOGIN_REQUEST_CODE), REQUEST_CODE_SIZE)
    length = pad_with_zeros(convert_int_to_bitarray(len(json.dumps(content).encode())), MESSAGE_LEN_SIZE)

    message = bitarray(request_code + length + binary_content)
    sock.sendall(message.tobytes())
    response_code = sock_bin_to_int(sock, 1)
    response_size = sock_bin_to_int(sock, 4)
    response = sock.recv(response_size)
    print(bson.loads(response))


def sock_bin_to_int(sock, size):
    response = bitarray(endian='big')
    temp = sock.recv(size)
    response.frombytes(temp)
    #print(response)
    response = ba2int(response, signed=False)
    print(response)
    return response


def main():
    print("Signup snd login")
    sock = connect_to_server(SERVER_IP, SERVER_PORT)
    with sock:
        send_signup_message(sock, "useraname", "password", "emailAddress@mail.com")
        send_login_message(sock, "useraname", "password")

    print("\n Signup with existing username")
    sock = connect_to_server(SERVER_IP, SERVER_PORT)
    with sock:
        send_signup_message(sock, "useraname", "password", "emailAddress@mail.com")

    print("\n login twice")
    sock = connect_to_server(SERVER_IP, SERVER_PORT)
    with sock:
        send_signup_message(sock, "usernameae", "password", "emailAddress@mail.com")
        send_login_message(sock, "usernameae", "password")
        send_login_message(sock, "usernameae", "password")


if __name__ == "__main__":
    main()
